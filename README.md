# suCGI

Run CGI scripts with the permissions of their owner.

suCGI checks whether a script is owned by a non-system user and within
the root directory of that user's home page, cleans up the environment,
and then runs the script with the permissions of that user.


## System requirements

suCGI should work on any recent-ish Unix-like system. More precisely,
it should work on any system that complies with [POSIX.1-2008], including
the X/Open System Interface extension, and is compatible with [4.4BSD].

Compiling suCGI requires:

* A C compiler that supports the [C99] standard
  (e.g., [GCC] ≥ v4.3 or [Clang] ≥ v1.0)
* An archiver, an assembler, and a linker
  (e.g., from [GNU Binutils] or a BSD system)
* Make ([GNU Make] and BSD makes are known to work)
* The header files of your system's standard library

suCGI comes with a script that installs
these applications if needed (see below).

[4.4BSD]: https://docs-legacy.freebsd.org/44doc/

[C99]: https://en.cppreference.com/w/c/99

[Clang]: https://clang.llvm.org/

[GCC]: https://gcc.gnu.org/

[GNU Binutils]: https://www.gnu.org/software/binutils/

[GNU Make]: https://www.gnu.org/software/make/

[POSIX.1-2008]: https://pubs.opengroup.org/onlinepubs/9699919799.2008edition/

## Installation

**WARNING:**
suCGI is work in progress and has *not* been reviewed,
let alone audited.

Download the repository and unpack it.

Please take the time to read and evaluate the source code.

Install a C build toolchain:

    sudo ./installc

*installc* uses the system's package manger.

Generate the build configuration by:

    ./configure webgroup=www-data cgidir=/usr/lib/cgi-bin

But replace "www-data" with name of the group that your web server runs as
and **/usr/lib/cgi-bin** with your web server's **/cgi-bin** directory.

See **[docs/build.rst]** and **[docs/install.md]**
for details and troubleshooting.

suCGI is configured at compile-time.
Copy **[config.h.tpl]** to **config.h** and adapt it to your needs:

    vi -c'read config.h.tpl' config.h

Compile suCGI by:

    make

Install suCGI by:

    sudo make install

See **[docs/install.md]** and **[docs/uninstall.md]** for details.

Optionally, remove the packages installed via *installc* by:

    sudo ./installc -r


[config.h.tpl]: config.h.tpl

[docs/build.rst]: docs/build.rst

[docs/install.md]: docs/install.md

[docs/uninstall.md]: docs/uninstall.md


## Setup

If you are are using [Apache] and want to enable users to run their [PHP]
scripts under their own user and group IDs, you can do so by following
these steps.

Enable [mod_userdir]:

    a2enmod userdir

Enable [mod_actions]:

    a2enmod actions

Enable **/cgi-bin**:

    a2enconf serve-cgi-bin

Add the following lines to your Apache configuration:

    <Directory "/home/*/public_html">
        <FilesMatch "[^.]+\.(php[3-8]?|phar|phtml)$">
            SetHandler sucgi
        </FilesMatch>
        Action sucgi /cgi-bin/sucgi
    </Directory>

The directory should correspond to USER_DIR in **config.h**.

Do *not* enable mod_php.

Restart Apache:

    apache2ctl -t && apache2ctl restart

[Apache]: https://httpd.apache.org/

[mod_actions]: https://httpd.apache.org/docs/2.4/mod/mod_actions.html

[mod_userdir]: https://httpd.apache.org/docs/2.4/mod/mod_userdir.html

[PHP]: https://www.php.net/


## Documentation

See the **docs** sub-directory and the source code.


## Contact

If you have discovered a potential security issue, please write an
email to the @zoho.com email address listed in my [PGP public key].
Do not let the PGP key put you off though; I would rather you
send me an unencrypted email than no email.

Note, bus errors, floating point exceptions, illegal instructions,
segmentation faults, bad system calls, and errors raised by the
address or the undefined behaviour sanitiser are all potential
security issues.

For anything else, please
[file an issue](https://codeberg.org/odkr/sucgi/issues).
But you are welcome to write me an email, too.

[GnuPG]: https://www.gnupg.org/

[PGP public key]: https://keys.openpgp.org/vks/v1/by-fingerprint/8975B184615BC48CFA4549056B06A2E03BE31BE9


## Licence

Copyright 2022, 2023, and 2024  Odin Kroeger

suCGI is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

suCGI is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
