/*
 * Test priv_drop
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

/* Needed for get getgrouplist */
#define _BSD_SOURCE
/* _DARWIN_C_SOURCE must NOT be set, or else getgroups will be pointless */
#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <pwd.h>
#include <grp.h>
#include <search.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../attr.h"
#include "../groups.h"
#include "../macros.h"
#include "../params.h"
#include "../priv.h"
#include "types.h"
#include "libutil/array.h"
#include "libutil/user.h"
#include "libutil/types.h"


/*
 * Constants
 */

/* Function signature format */
#define FNSIG "<ruid>=%lld <euid>=%lld --> (%lld, %lld, %d, <gids>)"


/*
 * Data types
 */

/* Mapping of arguments to return values */
typedef struct {
    uid_t ruid;         /* Initial real process UID */
    uid_t euid;         /* Initial effective process UID */
    uid_t targetuid;    /* UID to set */
    gid_t targetgid;    /* Primary GID to set */
    int ntargetgids;    /* Number of supplementary GIDs to set */
    Error retval;       /* Return value */
    int signal;         /* Signal raised, if any */
} PrivDropArgs;


/*
 * Main
 */

int
main(void)
{
    ASSERT(sizeof(GRP_T) == sizeof(gid_t));
    ASSERT(MAX_NGRPS_VAL >= INT_MAX);
    ASSERT((NGRPS_T) MAX_NGRPS_VAL == MAX_NGRPS_VAL);

    const uid_t superuid = 0;
    /* cppcheck-suppress unreadVariable; may not be read */
    const gid_t supergid _unused = 0;
    uid_t regularuid;
    uid_t regulargid;

    if (getuid() == 0) {
        struct passwd pwd;

        if (user_get_regular(&pwd, err) != 0) {
            errx(SKIP, "no regular user found");
        }

        regularuid = pwd.pw_uid;
        regulargid = pwd.pw_gid;
    } else {
        regularuid = getuid();
        regulargid = getgid();
    }

    const PrivDropArgs cases[] = {
/* Bad arguments */
#if !defined(NDEBUG)
        {
            .ruid = superuid,
            .euid = superuid,
            .targetuid = superuid,
            .targetgid = supergid,
            .ntargetgids = MAX_NGROUPS,
            .retval = OK,
            .signal = SIGABRT
        },
        {
            .ruid = superuid,
            .euid = superuid,
            .targetuid = regularuid,
            .targetgid = supergid,
            .ntargetgids = MAX_NGROUPS,
            .retval = OK,
            .signal = SIGABRT
        },
        {
            .ruid = regularuid,
            .euid = regularuid,
            .targetuid = superuid,
            .targetgid = supergid,
            .ntargetgids = MAX_NGROUPS,
            .retval = ERR_SYS,
            .signal = SIGABRT
        },
        {
            .ruid = regularuid,
            .euid = regularuid,
            .targetuid = regularuid,
            .targetgid = supergid,
            .ntargetgids = MAX_NGROUPS,
            .retval = ERR_SYS,
            .signal = SIGABRT
        },
#endif /* !defined(NDEBUG)*/

        {
            .ruid = superuid,
            .euid = superuid,
            .targetuid = regularuid,
            .targetgid = regulargid,
            .ntargetgids = MAX_NGROUPS,
            .retval = OK,
            .signal = 0
        },
        {
            .ruid = regularuid,
            .euid = superuid,
            .targetuid = regularuid,
            .targetgid = regulargid,
            .ntargetgids = MAX_NGROUPS,
            .retval = OK,
            .signal = 0
        },
        {
            .ruid = regularuid,
            .euid = superuid,
            .targetuid = regularuid,
            .targetgid = regulargid,
            .ntargetgids = 0,
            .retval = OK,
            .signal = 0
        },
        {
            .ruid = regularuid,
            .euid = superuid,
            .targetuid = regularuid,
            .targetgid = regulargid,
            .ntargetgids = 1,
            .retval = OK,
            .signal = 0
        },
        {
            .ruid = superuid,
            .euid = regularuid,
            .targetuid = regularuid,
            .targetgid = regulargid,
            .ntargetgids = MAX_NGROUPS,
            .retval = ERR_SYS,
            .signal = 0
        },
        {
            .ruid = regularuid,
            .euid = regularuid,
            .targetuid = regularuid,
            .targetgid = regulargid,
            .ntargetgids = MAX_NGROUPS,
            .retval = ERR_SYS,
            .signal = 0
        }
    };

    int result = PASS;

    for (size_t i = 0; i < NELEMS(cases); ++i) {
        const PrivDropArgs args = cases[i];

        errno = 0;
        /* cppcheck-suppress getpwuidCalled; used safely */
        const struct passwd *const targetuser = getpwuid(args.targetuid);
        if (targetuser == NULL) {
            /* cppcheck-suppress misra-c2012-22.10; getpwuid sets errno */
            if (errno == 0) {
                errx(ERROR, "UID %lld: no such user",
                     (long long) args.targetuid);
            } else {
                err(ERROR, "getpwuid");
            }
        }

        /* RATS: ignore; fork is used safely */
        const pid_t pid = fork();
        if (pid == 0) {
            if (geteuid() == 0) {
                gid_t args_egid;
                if (user_get_group(args.euid, &args_egid, err) != 0) {
                    errx(ERROR, "UID %lld: no such user",
                         (long long) args.euid);
                }

                gid_t args_rgid;
                if (user_get_group(args.ruid, &args_rgid, err) != 0) {
                    errx(ERROR, "UID %lld: no such user",
                         (long long) args.ruid);
                }

                errno = 0;
                if (setregid(args_rgid, args_egid) != 0) {
                    err(ERROR, "setregid");
                }

                errno = 0;
                if (setreuid(args.ruid, args.euid) != 0) {
                    err(ERROR, "setreuid");
                }
            } else {
                if (getuid() != args.ruid || geteuid() != args.euid) {
                    errx(SKIP, "skipping " FNSIG " ...",
                         (long long) args.ruid,
                         (long long) args.euid,
                         (long long) args.targetuid,
                         (long long) args.targetgid,
                         args.ntargetgids);
                }
            }

            gid_t targetgids[MAX_NGROUPS];
            int ntargetgids = NELEMS(targetgids);
            if (getgrouplist(targetuser->pw_name, (GRP_T) args.targetgid,
                             (GRP_T *) targetgids, &ntargetgids) < 0)
            {
                errx(SKIP, "user %s: belongs to too many groups.",
                     targetuser->pw_name);
            }

            if (args.signal != 0) {
                warnx("the next test should fail an assertion.");
            }

            const Error retval = priv_drop(args.targetuid, args.targetgid,
                                           (NGRPS_T) ntargetgids, targetgids);
            if (retval != args.retval) {
                errx(FAIL, FNSIG " -> %u [!]",
                     (long long) args.ruid,
                     (long long) args.euid,
                     (long long) args.targetuid,
                     (long long) args.targetgid,
                     args.ntargetgids,
                     retval);
            }

            if (retval != OK) {
                exit(0);
            }

            const uid_t ruid = getuid();
            if (ruid != args.targetuid) {
                errx(FAIL, FNSIG " --> <ruid> = %lld [!]",
                     (long long) args.ruid,
                     (long long) args.euid,
                     (long long) args.targetuid,
                     (long long) args.targetgid,
                     args.ntargetgids,
                     (long long) ruid);
            }

            const uid_t euid = geteuid();
            if (euid != args.targetuid) {
                errx(FAIL, FNSIG " --> <euid> = %lld [!]",
                     (long long) args.ruid,
                     (long long) args.euid,
                     (long long) args.targetuid,
                     (long long) args.targetgid,
                     args.ntargetgids,
                     (long long) euid);
            }

            const gid_t rgid = getgid();
            if (rgid != args.targetgid) {
                errx(FAIL, FNSIG " --> <rgid> = %lld [!]",
                     (long long) args.ruid,
                     (long long) args.euid,
                     (long long) args.targetuid,
                     (long long) args.targetgid,
                     args.ntargetgids,
                     (long long) euid);
            }

            const gid_t egid = getegid();
            if (egid != args.targetgid) {
                errx(FAIL, FNSIG " --> <egid> = %lld [!]",
                     (long long) args.ruid,
                     (long long) args.euid,
                     (long long) args.targetuid,
                     (long long) args.targetgid,
                     args.ntargetgids,
                     (long long) egid);
            }

            gid_t gids[MAX_NGROUPS];
            errno = 0;
            const int ngids = getgroups(MAX_NGROUPS, gids);
            if (ngids == -1) {
                err(ERROR, "getgroups");
            }

            if (ntargetgids == ngids) {
                if (!array_eq(gids, (size_t) ngids, sizeof(*gids),
                              targetgids, (size_t) ntargetgids,
                              sizeof(*targetgids), (CompFn) groups_comp)) {
                    errx(FAIL, FNSIG " --> supplementary GID is missing [!]",
                         (long long) args.ruid,
                         (long long) args.euid,
                         (long long) args.targetuid,
                         (long long) args.targetgid,
                         args.ntargetgids);
                }
            } else if (ntargetgids > ngids) {
                if (
                    !array_is_sub(
                        gids, (size_t) ngids, sizeof(*gids),
                        targetgids, (size_t) ntargetgids, sizeof(*targetgids),
                        (CompFn) groups_comp
                    )
                ) {
                    errx(FAIL, FNSIG " --> supplementary GID is wrong [!]",
                         (long long) args.ruid,
                         (long long) args.euid,
                         (long long) args.targetuid,
                         (long long) args.targetgid,
                         args.ntargetgids);
                }
            } else {
                errx(FAIL, FNSIG " --> too many supplementary GIDs set",
                     (long long) args.ruid,
                     (long long) args.euid,
                     (long long) args.targetuid,
                     (long long) args.targetgid,
                     args.ntargetgids);
            }

            /*
             * Tests are run by this process, so coverage
             * data should be written by this process, too.
             */
            exit(0);
        } else {
            int status;

            do {
                errno = 0;
                if (waitpid(pid, &status, 0) >= 0) {
                    break;
                }
                /* cppcheck-suppress misra-c2012-22.10; waitpid sets errno */
                if (errno != EINTR) {
                    err(ERROR, "waitpid %d", pid);
                }
            } while (true);

            /* cppcheck-suppress misra-c2012-14.4; return value is boolean */
            if (WIFEXITED(status)) {
                int exitstatus = WEXITSTATUS(status);

                switch (exitstatus) {
                case PASS:
                    break;
                case FAIL:
                    result = FAIL;
                    break;
                case SKIP:
                    if (result == (int) PASS) {
                        result = SKIP;
                    }
                    break;
                default:
                    return ERROR;
                }
            /* cppcheck-suppress misra-c2012-14.4; return value is boolean */
            } else if (WIFSIGNALED(status)) {
                int signal = WTERMSIG(status);

                if (signal != args.signal) {
                    result = FAIL;
                    warnx(FNSIG " ^ %s [!]",
                          (long long) args.ruid,
                          (long long) args.euid,
                          (long long) args.targetuid,
                          (long long) args.targetgid,
                          args.ntargetgids,
                          strsignal((int) signal));
                }
            } else {
                errx(ERROR, "child %d exited abnormally", pid);
            }
        }
    }

    /* Running atexit functions in this process breaks coverage reports */
    _exit(result);
}
