/*
 * Test groups_are_sub
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <err.h>
#include <setjmp.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../env.h"
#include "../groups.h"
#include "../macros.h"
#include "types.h"
#include "libutil/abort.h"
#include "libutil/types.h"


/*
 * Data types
 */

/* Mapping of a string to a return value */
typedef struct {
    size_t nsub;
    const gid_t *sub;
    size_t nsuper;
    const gid_t *super;
    bool retval;
    int signal;
} GroupsAreSubArgs;


/*
 * Main
 */

int
main(void)
{
    const GroupsAreSubArgs cases[] = {
        /* Bad arguments */
#if !defined(NDEBUG) && defined(NATTR)
        {
            .nsub = 0,
            .sub = NULL,
            .nsuper = 0,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = true,
            .signal = SIGABRT
        },
        {
            .nsub = 0,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 0,
            .super = NULL,
            .retval = true,
            .signal = SIGABRT
        },
#endif

        /* Empty subset */
        {
            .nsub = 0,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 0,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = true,
            .signal = 0
        },
        {
            .nsub = 0,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 1,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = true,
            .signal = 0
        },
        {
            .nsub = 0,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 2,
            .super = (gid_t []) {0, (gid_t) NOGROUP},
            .retval = true,
            .signal = 0
        },
        {
            .nsub = 0,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 3,
            .super = (gid_t []) {0, 1, (gid_t) NOGROUP},
            .retval = true,
            .signal = 0
        },

        /* Empty superset */
        {
            .nsub = 1,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 0,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 2,
            .sub = (gid_t []) {0, (gid_t) NOGROUP},
            .nsuper = 0,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 3,
            .sub = (gid_t []) {0, 1, (gid_t) NOGROUP},
            .nsuper = 0,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 1,
            .sub = (gid_t []) {MAX_GID_VAL},
            .nsuper = 0,
            .super = (gid_t []) {MAX_GID_VAL},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 2,
            .sub = (gid_t []) {0, MAX_GID_VAL},
            .nsuper = 0,
            .super = (gid_t []) {MAX_GID_VAL},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 3,
            .sub = (gid_t []) {0, 1, MAX_GID_VAL},
            .nsuper = 0,
            .super = (gid_t []) {MAX_GID_VAL},
            .retval = false,
            .signal = 0
        },

        /* False */
        {
            .nsub = 1,
            .sub = (gid_t []) {(gid_t) NOGROUP},
            .nsuper = 3,
            .super = (gid_t []) {0, 1, 2},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 3,
            .sub = (gid_t []) {1, 2, 3},
            .nsuper = 1,
            .super = (gid_t []) {0},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 3,
            .sub = (gid_t []) {1, 2, 3},
            .nsuper = 1,
            .super = (gid_t []) {(gid_t) NOGROUP},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 3,
            .sub = (gid_t []) {1, 2, 3},
            .nsuper = 1,
            .super = (gid_t []) {MAX_GID_VAL},
            .retval = false,
            .signal = 0
        },
        {
            .nsub = 3,
            .sub = (gid_t []) {0, 1, (gid_t) NOGROUP},
            .nsuper = 4,
            .super = (gid_t []) {0, 1, 3 ,4, MAX_GID_VAL},
            .retval = false,
            .signal = 0
        },

        /* True */
        {
            .nsub = 3,
            .sub = (gid_t []) {0, 1, 2, (gid_t) NOGROUP, MAX_GID_VAL},
            .nsuper = 4,
            .super = (gid_t []) {0, 1, 2, (gid_t) NOGROUP, MAX_GID_VAL},
            .retval = true,
            .signal = 0
        },
        {
            .nsub = 1,
            .sub = (gid_t []) {0},
            .nsuper = 3,
            .super = (gid_t []) {0, 1, 2, (gid_t) NOGROUP, MAX_GID_VAL},
            .retval = true,
            .signal = 0
        },
        {
            .nsub = 1,
            .sub = (gid_t []) {0},
            .nsuper = 1,
            .super = (gid_t []) {0},
            .retval = true,
            .signal = 0
        }
    };

    volatile int result = PASS;

    for (volatile size_t i = 0; i < NELEMS(cases); ++i) {
        const GroupsAreSubArgs args = cases[i];

        if (sigsetjmp(abort_env, 1) == 0) {
            if (args.signal != 0) {
                warnx("the next test should fail an assertion.");
            }

            (void) abort_catch(err);
            const bool retval = groups_are_sub(args.nsub, args.sub,
                                               args.nsuper, args.super);
            (void) abort_reset(err);

            if (args.retval != retval) {
                result = FAIL;
                warnx("(%zu, <sub>, %zu, <super>) -> %d [!]",
                      args.nsub, args.nsuper, retval);
            }
        }

        if (abort_signal != args.signal) {
            result = FAIL;
            warnx("(%zu, <sub>, %zu, <super>) ^ %s [!]",
                  args.nsub, args.nsuper, strsignal((int) abort_signal));
        }
    }

    return result;
}
