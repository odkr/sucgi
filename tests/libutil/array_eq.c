/*
 * Check whether two arrays are non-strict subsets of each other
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#include "array.h"
#include "types.h"

/* NOLINTBEGIN(readability-suspicious-call-argument); clear enough */

bool
array_eq(const void *const a, const size_t alen, const size_t awidth,
         const void *const b, const size_t blen, const size_t bwidth,
         const CompFn cmp)
{
    assert(a != NULL);
    assert(awidth > 0U);
    assert(b != NULL);
    assert(bwidth > 0U);

    return array_is_sub(a, alen, awidth, b, blen, bwidth, cmp) &&
           array_is_sub(b, blen, bwidth, a, alen, awidth, cmp);
}

/* NOLINTEND(readability-suspicious-call-argument) */
