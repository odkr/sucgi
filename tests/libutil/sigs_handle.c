/*
 * Register the given handler to handle the given signals
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <assert.h>
#include <signal.h>
#include <stdlib.h>

#include "types.h"
#include "sigs.h"

int
sigs_handle(void (*func)(int),
            const size_t nsignals, const int *const signals,
            Trap *const old, const ErrorFn errh)
{
    assert(func != NULL);
    assert(signals != NULL);
    const struct sigaction action = {.sa_handler = func};
    return sigs_action(&action, nsignals, signals, old, errh);
}
