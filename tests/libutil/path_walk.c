/*
 * Cycle over the segments of a path
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

/* Some flags to open are non-standard */
#define _BSD_SOURCE
#define _DARWIN_C_SOURCE
#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <sys/types.h>
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>

#include "path.h"
#include "sigs.h"
#include "types.h"


/*
 * Macros
 */

/* Open flag for entering directories */
#if defined(O_SEARCH)
#define OFLAG_SEARCH O_SEARCH
#elif defined(O_EXEC)
#define OFLAG_SEARCH O_EXEC
#elif defined(O_PATH)
#define OFLAG_SEARCH O_PATH
#else
#define OFLAG_SEARCH O_RDONLY
#endif


/*
 * Functions
 */

int
/* cppcheck-suppress misra-c2012-8.7; path_walk is used externally */
path_walk(const char *const fname,
          const VFileFn dirfn, const VFileFn basefn,
          const ErrorFn errh,
          const size_t nargs, ...)
{
    assert(fname != NULL);
    assert(*fname != '\0');
    assert(dirfn != NULL || basefn != NULL);

    int retval = 0;
    int fatalerr = 0;

    /*
     * NOLINTBEGIN(misc-redundant-expression);
     * `O_SEARCH | O_DIRECTORY` is not redundant, POSIX.1-2008 leaves
     * the result of `open(<non-directory>, O_SEARCH)` unspecified.
     */

    errno = 0;
    /* RATS: ignore; filename cannot be wrong */
    int oldwd = open(".", OFLAG_SEARCH | O_DIRECTORY | O_CLOEXEC);
    if (oldwd < 0) {
        return -1;
    }

    /* NOLINTEND(misc-redundant-expression) */

    const char *ptr = fname;
    char *seg = NULL;

    /* cppcheck-suppress misra-c2012-15.4 */
    while ((errno = 0, seg = path_split(ptr, &ptr, NULL)) != NULL) {
        const bool isdir = (*ptr != '\0');
        const VFileFn func = isdir ? dirfn : basefn;

        if (func != NULL) {
            va_list argp;

            va_start(argp, nargs);
            errno = 0;
            retval = func(seg, nargs, argp);
            va_end(argp);

            if (retval != 0) {
                goto cleanup;
            }
        }

        if (isdir) {
            errno = 0;
            retval = chdir(seg);

            if (retval != 0) {
                goto cleanup;
            }
        }

        cleanup:
            free(seg);

        if (retval != 0) {
            break;
        }
    }

    /* cppcheck-suppress misra-c2012-22.10; path_split sets errno */
    if (errno != 0) {
        fatalerr = errno;
        retval = -1;
    }

    if (sigs_retry_i(fchdir, oldwd) != 0) {
        /* Being in another directory than you think is BAD */
        warn("%s:%d: chdir %s", __FILE__, __LINE__, fname);
        _exit(EXIT_FAILURE);
    }

    errno = 0;
    if (sigs_retry_i(close, oldwd) != 0) {
        /* NOTREACHED */
        if (retval == 0) {
            fatalerr = errno;
            retval = -1;
        }
    }

    errno = fatalerr;
    if (retval != 0 && errh != NULL) {
        errh(EXIT_FAILURE, "path_walk %s", fname);
    }

    return retval;
}
