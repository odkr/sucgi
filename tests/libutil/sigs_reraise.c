/*
 * Reset the handler for the given signal to its default and raise it
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <errno.h>
#include <signal.h>
#include <stdlib.h>

#include "types.h"
#include "sigs.h"

int
sigs_reraise(const int signal, const ErrorFn errh)
{
    static const struct sigaction dflaction = {.sa_handler = SIG_DFL};

    errno = 0;
    if (sigaction(signal, &dflaction, NULL) != 0) {
        if (errh != NULL) {
            errh(EXIT_FAILURE, "%s:%d: sigaction", __FILE__, __LINE__);
        }
        return -1;
    }

    errno = 0;
    if (raise(signal) != 0) {
        if (errh != NULL) {
            errh(EXIT_FAILURE, "%s:%d: raise", __FILE__, __LINE__);
        }
        return -1;
    }

    return 0;
}
