/*
 * Call a function repeatedly until it's not interrupted by a signal
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <assert.h>
#include <errno.h>

#include "sigs.h"

int
sigs_retry_i(int (*const func)(int), const int val)
{
    assert(func != NULL);

    int retval = 0;

    do {
        errno = 0;
        retval = func(val);
    } while (retval != 0 &&
             /* cppcheck-suppress misra-c2012-22.10 */
             errno == EINTR);

    return retval;
}
