/*
 * Array search and comparison
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#if !defined(TESTS_UTIL_ARRAY_H)
#define TESTS_UTIL_ARRAY_H

#include <sys/types.h>
#include <stdbool.h>

#include "../../attr.h"
#include "types.h"

/* NOLINTBEGIN(readability-suspicious-call-argument); clear enough */

/*
 * Search the given haystack for the given needle.
 *
 * The haystack must have at least as many elements as the given length,
 * and each element must be exactly as wide as the given width;
 * supernumery elements are ignored.
 *
 * Elements are compared by calling the given comparison function.
 *
 * Return value:
 *      NULL        No element in haystack matches.
 *      Non-Null    Pointer to the matching element.
 */
_read_only(1, 4) _read_only(2, 3) _nonnull(1, 2, 5) _nodiscard
const void *array_find(const void *needle, const void *haystack,
                       size_t len, size_t width, CompFn cmp);

/*
 * Check whether one array is a non-strict subset of another.
 *
 * Each array must have at least as many elements as the given length,
 * and each element must be as wide as the given width;
 * supernumery elements are ignored.
 *
 * Elements are compared by calling the given comparison function.
 */
_read_only(1, 2) _read_only(4, 5) _nonnull(1, 4, 7) _nodiscard
bool array_is_sub(const void *sub, size_t sublen, size_t subwidth,
                  const void *super, size_t superlen, size_t superwidth,
                  CompFn cmp);

/*
 * Check whether two arrays are non-strict subsets of each other.
 *
 * Otherwise the same as array_is_sub.
 */
_read_only(1, 2) _read_only(4, 5) _nonnull(1, 4, 7) _nodiscard
bool array_eq(const void *a, size_t alen, size_t awidth,
              const void *b, size_t blen, size_t bwidth,
              CompFn cmp);

/* NOLINTEND(readability-suspicious-call-argument) */

#endif /* !defined(TESTS_UTIL_ARRAY_H) */
