/*
 * Get a user's primary group
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <pwd.h>
#include <stdlib.h>
#include <unistd.h>

#include "../../params.h"
#include "user.h"
#include "types.h"


/*
 * Macros
 */

/* Default buffer size */
#define BUFSIZE 8192


/*
 * Functions
 */

int
user_get_group(const uid_t user, gid_t *const group, const ErrorFn errh)
{
    assert(group != NULL);

    /* Set GID to an invalid value */
    *group = (gid_t) NOGROUP;

    errno = 0;
    long bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
    if (bufsize < 0) {
        /* cppcheck-suppress misra-c2012-22.10; sysconf may set errno */
        if (errno != 0) {
            if (errh != NULL) {
                errh(EXIT_FAILURE, "sysconf");
            }

            return -1;
        }

        bufsize = BUFSIZE;
    }

    while ((size_t) bufsize == (unsigned long) bufsize) {
        errno = 0;
        /* cppcheck-suppress misra-c2012-11.5; bad advice for malloc */
        char *buf = malloc((size_t) bufsize);
        if (buf == NULL) {
            if (errh != NULL) {
                errh(EXIT_FAILURE, "malloc");
            }

            return -1;
        }

        struct passwd pwd;
        struct passwd *result = NULL;
        errno = getpwuid_r(user, &pwd, buf, (size_t) bufsize, &result);

        /* Nothing is read from the buffer */
        free(buf);

        if (result != NULL) {
            *group = pwd.pw_gid;
            return 0;
        }

        /* cppcheck-suppress misra-c2012-22.10; errno has been set */
        switch (errno) {
        default:
            break;
        case 0:
            return 1;
        case ERANGE:
            if (LONG_MAX / bufsize <= 2) {
                bufsize *= 2;
                continue;
            }
            /* Falls through */
        }

        break;
    }

    if (errh != NULL) {
        errh(EXIT_FAILURE, "getpwuid");
    }

    return -1;
}
