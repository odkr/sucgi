/*
 * Register the given action to handle the given signals
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <assert.h>
#include <signal.h>

#include "types.h"
#include "sigs.h"


/*
 * Macros
 */

/* Number of signals on the system */
#if !defined(NSIG)
#if defined(_NSIG)
#define NSIG _NSIG
#else
#define NSIG 128
#endif
#endif


/*
 * Functions
 */

int
sigs_action(const struct sigaction *const action,
            const size_t nsignals, const int *const signals,
            Trap *const old, const ErrorFn errh)
{
    assert(action != NULL);
    assert(signals != NULL);

    int retval = -1;

    Trap traps[NSIG];
    for (size_t i = 0; i < nsignals; ++i) {
        traps[i] = (Trap) {.signal = signals[i], .action = action};
    }

    retval = sigs_trap(nsignals, traps, old, errh);

    return retval;
}
