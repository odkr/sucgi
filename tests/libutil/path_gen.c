/*
 * Generate a path-like string
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <sys/types.h>
#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "path.h"
#include "sigs.h"
#include "types.h"

/*
 * Macros
 */

/* Length of path segments */
#define NAME_LEN _POSIX_NAME_MAX


/*
 * Function
 */

void
path_gen(const size_t size, char *const path) {
    assert(NAME_LEN > 0);
    assert(path != NULL);

    const size_t len = size - 1U;

    (void) memset(path, 'x', len);

    /* - 1U so that the last character is never a '/' */
    for (size_t i = NAME_LEN - 1; i < len - 1U; i += NAME_LEN) {
        path[i] = '/';
    }

    path[len] = '\0';
}
