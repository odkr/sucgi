/*
 * Test env_restore
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <regex.h>
#include <setjmp.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../env.h"
#include "../macros.h"
#include "../params.h"
#include "../str.h"
#include "types.h"
#include "libutil/abort.h"
#include "libutil/array.h"
#include "libutil/str.h"
#include "libutil/types.h"



/*
 * Constants
 */

/* Maximum number of environment variables for testing */
#define MAX_TEST_NVARS (MAX_NVARS + 1)

/* Maximum string length for testing */
#define MAX_TEST_STR_LEN (MAX_STR_LEN + 1U)


/*
 * Data types
 */

/* Mapping of constant inputs to constant outputs */
typedef struct {
    const char *const *vars;        /* Variables to set */
    size_t npatterns;               /* Number of patterns */
    const char *const *patterns;    /* Patterns */
    const char *const *env;         /* Resulting environmnet */
    Error retval;                   /* Return value */
    int signal;                     /* Signal caught, if any */
} EnvRestoreArgs;


/*
 * Main
 */

int
main(void)
{
    /* RATS: ignore; used safely */
    const char *const safe_env_vars[] = SAFE_ENV_VARS;

    /* RATS: ignore; used safely */
    char *hugenvars[MAX_NVARS + 1];

    /* RATS: ignore; used safely */
    char hugevars[MAX_NVARS + 1][MAX_VAR_LEN];

    for (ptrdiff_t i = 0; i < MAX_NVARS; ++i) {
        errno = 0;
        /* RATS: ignore; format is a literal and expansion is bounded */
        int nbytes = snprintf(hugevars[i], MAX_VAR_LEN, "var%td=", i);
        if (nbytes < 1) {
            err(ERROR, "snprintf");
        }

        hugenvars[i] = hugevars[i];
    }
    hugenvars[MAX_NVARS] = NULL;

    /* RATS: ignore; used safely */
    char longvar[MAX_VAR_LEN] = {0};
    (void) memset(longvar, 'x', sizeof(longvar) - 1U);
    longvar[1] = '=';

    /* RATS: ignore; used safely */
    char hugevar[MAX_VAR_LEN + 1] = {0};
    (void) memset(hugevar, 'x', sizeof(hugevar) - 1U);
    hugevar[1] = '=';

    /* RATS: ignore; used safely */
    char longname[MAX_VAR_LEN] = {0};
    (void) memset(longname, 'x', sizeof(longname) - 1U);
    longname[MAX_VARNAME_LEN - 1U] = '=';

    /* RATS: ignore; used safely */
    char hugename[MAX_VAR_LEN] = {0};
    (void) memset(hugename, 'x', sizeof(hugename) - 1U);
    hugename[MAX_VARNAME_LEN] = '=';

    const EnvRestoreArgs cases[] = {
        /* Bad arguments */
#if !defined(NDEBUG) && defined(NATTR)
        {
            .vars = NULL,
            .npatterns = 0,
            .patterns = (const char * []) {NULL},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = SIGABRT
        },
#endif

        /* Null tests */
        {
            .vars = (const char * []) {NULL},
            .npatterns = 0,
            .patterns = (const char * []) {NULL},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* Too many variables */
        {
            .vars = (const char *const *) hugenvars,
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = ERR_NELEMS,
            .signal = 0
        },

        /* Overly long variables should be ignored */
        {
            .vars = (const char * []) {longvar, NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {longvar, NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {hugevar, NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* Variables with overly long should be ignored, too */
        {
            .vars = (const char * []) {longname, NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {longname, NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {hugename, NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* Simple tests */
        {
            .vars = (const char * []) {"foo=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {"foo=foo", NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"^foo$"},
            .env = (const char * []) {"foo=foo", NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                "foobar=foobar",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"^foo"},
            .env = (const char * []) {
                "foo=foo",
                "foobar=foobar",
                NULL
            },
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                "foobar=foobar",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"foo"},
            .env = (const char * []) {
                "foo=foo",
                "foobar=foobar",
                NULL
            },
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                "foobar=foobar",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"bar$"},
            .env = (const char * []) {
                "bar=bar",
                "foobar=foobar",
                NULL
            },
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                "foobar=foobar",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"bar"},
            .env = (const char * []) {
                "bar=bar",
                "foobar=foobar",
                NULL
            },
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                "baz=baz",
                NULL
            },
            .npatterns = 0,
            .patterns = (const char * []) {NULL},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo=foo",
                "bar=bar",
                "baz=baz",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"^$"},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* Syntax errors */
        {
            .vars = (const char * []) {"", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* Illegal names */
        {
            .vars = (const char * []) {" foo=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"0foo=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"*=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"foo =foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"$(foo)=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"`foo`=foo", NULL},
            .npatterns = 1,
            .patterns = (const char * []) {"."},
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* More realistic tests */
        {
            .vars = (const char * []) {
                "bar=bar",
                "foo_0=foo",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"^foo_[0-9]+$"},
            .env = (const char * []) {"foo_0=foo", NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "bar=bar",
                "foo_0=foo",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"^foo_(0|[1-9][0-9]*)$"},
            .env = (const char * []) {"foo_0=foo", NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "foo_0=foo",
                "foo_01=foo",
                NULL
            },
            .npatterns = 1,
            .patterns = (const char * []) {"^foo_(0|[1-9][0-9]*)$"},
            .env = (const char * []) {"foo_0=foo", NULL},
            .retval = OK,
            .signal = 0
        },

        /* Simple tests with real patterns */
        {
            .vars = (const char * []) {"foo=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"PATH_TRANSLATED=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {"PATH_TRANSLATED=foo", NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "PATH_TRANSLATED=foo",
                "foo=foo",
                NULL
            },
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {"PATH_TRANSLATED=foo", NULL},
            .retval = OK,
            .signal = 0
        },

        /* Illegal names */
        {
            .vars = (const char * []) {" IPV6=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"0IPV6=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"*=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"IPV6 =foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"$(IPV6)=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {"`IPV6`=foo", NULL},
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },

        /* Odd but legal values */
        {
            .vars = (const char * []) {
                "SSL_CLIENT_S_DN_C_0=",
                "SSL_CLIENT_S_DN_C_1==",
                "SSL_CLIENT_S_DN_C_2= ",
                "SSL_CLIENT_S_DN_C_3=\t",
                "SSL_CLIENT_S_DN_C_4=\n",
                NULL
            },
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {
                "SSL_CLIENT_S_DN_C_0=",
                "SSL_CLIENT_S_DN_C_1==",
                "SSL_CLIENT_S_DN_C_2= ",
                "SSL_CLIENT_S_DN_C_3=\t",
                "SSL_CLIENT_S_DN_C_4=\n",
                NULL
            },
            .retval = OK,
            .signal = 0
        },

        /* Real-world tests */
        {
            .vars = (const char * []) {
                "CLICOLOR=x",
                "EDITOR=vim",
                "HOME=/home/jdoe",
                "IFS=",
                "LANG=en_GB.UTF-8",
                "LOGNAME=jdoe",
                "OLDPWD=/home",
                "PAGER=less",
                "PATH=/bin:/usr/bin",
                "PWD=/home/jdoe",
                "SHELL=/bin/zsh",
                "USER=jdoe",
                "VISUAL=vim",
                NULL
            },
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {NULL},
            .retval = OK,
            .signal = 0
        },
        {
            .vars = (const char * []) {
                "CLICOLOR=x",
                "DOCUMENT_ROOT=/home/jdoe/public_html",
                "EDITOR=vim",
                "HOME=/home/jdoe",
                "HTTP_HOST=www.foo.example",
                "HTTP_REFERER=https://www.bar.example",
                "HTTP_USER_AGENT=FakeZilla/1",
                "HTTPS=on",
                "IFS=",
                "LANG=en_GB.UTF-8",
                "LOGNAME=jdoe",
                "OLDPWD=/home",
                "PAGER=less",
                "PATH_TRANSLATED=/home/jdoe/public_html/index.php",
                "PATH=/bin:/usr/bin",
                "PWD=/home/jdoe",
                "QUERY_STRING=foo=bar&bar=baz",
                "REMOTE_ADDR=100::1:2:3",
                "REMOTE_HOST=100::1:2:3",
                "REMOTE_PORT=50000",
                "REQUEST_METHOD=GET",
                "REQUEST_URI=/index.php",
                "SCRIPT_NAME=index.php",
                "SERVER_ADMIN=admin@foo.example",
                "SERVER_NAME=www.foo.example",
                "SERVER_PORT=443",
                "SERVER_SOFTWARE=Apache v2.4",
                "SHELL=/bin/zsh",
                "TMPDIR=/tmp/user/1000",
                "USER=jdoe",
                "VISUAL=vim",
                NULL
            },
            .npatterns = NELEMS(safe_env_vars),
            .patterns = safe_env_vars,
            .env = (const char * []) {
                "HTTP_HOST=www.foo.example",
                "HTTP_REFERER=https://www.bar.example",
                "HTTP_USER_AGENT=FakeZilla/1",
                "HTTPS=on",
                "PATH_TRANSLATED=/home/jdoe/public_html/index.php",
                "QUERY_STRING=foo=bar&bar=baz",
                "REMOTE_ADDR=100::1:2:3",
                "REMOTE_HOST=100::1:2:3",
                "REMOTE_PORT=50000",
                "REQUEST_METHOD=GET",
                "REQUEST_URI=/index.php",
                "SCRIPT_NAME=index.php",
                "SERVER_ADMIN=admin@foo.example",
                "SERVER_NAME=www.foo.example",
                "SERVER_PORT=443",
                "SERVER_SOFTWARE=Apache v2.4",
                "TMPDIR=/tmp/user/1000",
                NULL
            },
            .retval = OK,
            .signal = 0
        }
    };

    volatile int result = PASS;
    char *null = NULL;

    /* Run tests */
    for (volatile size_t i = 0; i < NELEMS(cases); ++i) {
        const EnvRestoreArgs args = cases[i];

        if (sigsetjmp(abort_env, 1) == 0) {
            environ = &null;

            regex_t pregs[MAX_TEST_NVARS] = {0};
            for (size_t j = 0; j < args.npatterns; ++j) {
                const int err = regcomp(&pregs[j], args.patterns[j],
                                        REG_EXTENDED | REG_NOSUB);

                if (err != 0) {
                    /* RATS: ignore; used safely */
                    char errmsg[MAX_ERRMSG_LEN];

                    (void) regerror(err, &pregs[j], errmsg, sizeof(errmsg));
                    errx(ERROR, "%s:%d: regcomp: %s",
                         __FILE__, __LINE__, errmsg);
                }
            }

            if (args.signal != 0) {
                warnx("the next test should fail an assertion.");
            }

            (void) abort_catch(err);
            const Error retval = env_restore(args.vars, args.npatterns, pregs);
            (void) abort_reset(err);

            /* Superfluous, but some versions of ASan think otherwise */
            for (size_t j = 0; j < args.npatterns; ++j) {
                regfree(&pregs[j]);
            }

            if (retval != args.retval) {
                result = FAIL;
                warnx("(<vars>, %zu, <pregs>) -> %u [!]",
                      args.npatterns, retval);
            }

            if (retval == OK) {
                volatile size_t nexpected = 0;
                while (args.env[nexpected] != NULL) {
                    assert(nexpected < SIZE_MAX);
                    ++nexpected;
                }

                volatile size_t nactual = 0;
                while (environ[nactual] != NULL) {
                    assert(nexpected < SIZE_MAX);
                    ++nactual;
                }

                if (!array_eq(
                    (const void *) args.env, nexpected, sizeof(*args.env),
                    (const void *) environ, nactual, sizeof(*environ),
                    (CompFn) str_cmp_ptrs
                )) {
                    result = FAIL;
                    warnx("(<vars>, %zu, <pregs>) --> <environ> [!]",
                          args.npatterns);
                }
            }
        }

        if (abort_signal != args.signal) {
            result = FAIL;
            warnx("(<vars>, %zu, <pregs>) ^ %s [!]",
                  args.npatterns, strsignal((int) abort_signal));
        }
    }

    return result;
}
