/*
 * Data types
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#if !defined(TESTS_TYPES_H)
#define TESTS_TYPES_H

/* Exit statuses for tests */
typedef enum {  /* cppcheck-suppress [misra-c2012-2.3, misra-c2012-2.4] */
    PASS  =  0,     /* Test passed */
    ERROR = 69,     /* An error occured */
    FAIL  = 70,     /* Test failed */
    SKIP  = 75      /* Test was skipped */
} Result;

#endif /* !defined(TESTS_TYPES_H) */
