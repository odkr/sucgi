/*
 * Test groups_find
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <err.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../env.h"
#include "../groups.h"
#include "../macros.h"
#include "types.h"
#include "libutil/abort.h"
#include "libutil/types.h"


/*
 * Data types
 */

/* Mapping of a string to a return value */
typedef struct {
    size_t ngroups;
    const gid_t *groups;
    gid_t *retval;
    gid_t group;
    int signal;
} GroupsFindArgs;


/*
 * Main
 */

int
main(void)
{
    const GroupsFindArgs cases[] = {
        /* Bad arguments */
#if !defined(NDEBUG) && defined(NATTR)
        {
            .group = 0,
            .ngroups = 1,
            .groups = NULL,
            .retval = NULL,
            .signal = SIGABRT
        },
#endif

        {
            .group = 0,
            .ngroups = 1,
            .groups = (const gid_t []) {0},
            .retval = (gid_t []) {0},
            .signal = 0
        },
        {
            .group = 1,
            .ngroups = 1,
            .groups = (const gid_t []) {1},
            .retval = (gid_t []) {1},
            .signal = 0
        },
        {
            .group = MAX_GID_VAL,
            .ngroups = 1,
            .groups = (const gid_t []) {MAX_GID_VAL},
            .retval = (gid_t []) {MAX_GID_VAL},
            .signal = 0
        },
        {
            .group = 0,
            .ngroups = 3,
            .groups = (const gid_t []) {0, 1, MAX_GID_VAL},
            .retval = (gid_t []) {0},
            .signal = 0
        },
        {
            .group = 1,
            .ngroups = 3,
            .groups = (const gid_t []) {0, 1, MAX_GID_VAL},
            .retval = (gid_t []) {1},
            .signal = 0
        },
        {
            .group = MAX_GID_VAL,
            .ngroups = 3,
            .groups = (const gid_t []) {0, 1, MAX_GID_VAL},
            .retval = (gid_t []) {MAX_GID_VAL},
            .signal = 0
        },
        {
            .group = 0,
            .ngroups = 1,
            .groups = (const gid_t []) {1},
            .retval = NULL,
            .signal = 0
        },
        {
            .group = 1,
            .ngroups = 1,
            .groups = (const gid_t []) {0},
            .retval = NULL,
            .signal = 0
        },
        {
            .group = MAX_GID_VAL,
            .ngroups = 1,
            .groups = (const gid_t []) {1},
            .retval = NULL,
            .signal = 0
        },
        {
            .group = 0,
            .ngroups = 2,
            .groups = (const gid_t []) {1, MAX_GID_VAL},
            .retval = NULL,
            .signal = 0
        },
        {
            .group = 1,
            .ngroups = 2,
            .groups = (const gid_t []) {0, MAX_GID_VAL},
            .retval = NULL,
            .signal = 0
        },
        {
            .group = MAX_GID_VAL,
            .ngroups = 2,
            .groups = (const gid_t []) {0, 1},
            .retval = NULL,
            .signal = 0
        }
    };

    volatile int result = PASS;

    for (volatile size_t i = 0; i < NELEMS(cases); ++i) {
        const GroupsFindArgs args = cases[i];

        if (sigsetjmp(abort_env, 1) == 0) {
            if (args.signal != 0) {
                warnx("the next test should fail an assertion.");
            }

            (void) abort_catch(err);
            const gid_t *const retval = groups_find(args.group,
                                                    args.ngroups,
                                                    args.groups);
            (void) abort_reset(err);

            if (args.retval == NULL) {
                if (retval != NULL) {
                    result = FAIL;
                    warnx("(%llu, %zu, <groups>) -> %p [!]",
                          (unsigned long long) args.group, args.ngroups,
                          (const void *) retval);
                }
            } else {
                if (*args.retval != *retval) {
                    result = FAIL;
                    warnx("(%llu, %zu, <groups>) -> %llu [!]",
                          (unsigned long long) args.group, args.ngroups,
                          (unsigned long long) *retval);
                }
            }
        }

        if (abort_signal != args.signal) {
            result = FAIL;
            warnx("(%llu, %zu, <groups>) ^ %s [!]",
                  (unsigned long long) args.group, args.ngroups,
                  strsignal((int) abort_signal));
        }
    }

    return result;
}
