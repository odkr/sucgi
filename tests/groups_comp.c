/*
 * Test groups_comp
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <err.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../env.h"
#include "../groups.h"
#include "../macros.h"
#include "types.h"
#include "libutil/abort.h"
#include "libutil/types.h"


/*
 * Data types
 */

/* Mapping of a string to a return value */
typedef struct {
    const gid_t gid1;
    const gid_t gid2;
    int retval;
} GroupsCompArgs;


/*
 * Main
 */

int
main(void)
{
    const GroupsCompArgs cases[] = {
        {0,                 0,                  0},
        {(gid_t) NOGROUP,   (gid_t) NOGROUP,    0},
        {MAX_GRP_VAL,       MAX_GRP_VAL,        0},
        {MAX_GID_VAL,       MAX_GID_VAL,        0},
        {0,                 (gid_t) NOGROUP,   -1},
        {(gid_t) NOGROUP,   0,                  1},
        {0,                 MAX_GRP_VAL,       -1},
        {MAX_GRP_VAL,       0,                  1},
        {0,                 MAX_GID_VAL,       -1},
        {MAX_GID_VAL,       0,                  1},
    };

    volatile int result = PASS;

    for (volatile size_t i = 0; i < NELEMS(cases); ++i) {
        const GroupsCompArgs args = cases[i];

        if (sigsetjmp(abort_env, 1) == 0) {
            (void) abort_catch(err);
            const int retval = groups_comp(&args.gid1, &args.gid2);
            (void) abort_reset(err);

            if (retval != args.retval) {
                result = FAIL;
                warnx("(%llu, %llu) -> %d [!]",
                      (unsigned long long) args.gid1,
                      (unsigned long long) args.gid2,
                      retval);
            }
        }

        if (abort_signal != 0) {
            result = FAIL;
            warnx("(%llu, %llu) ^ %s [!]",
                  (unsigned long long) args.gid1,
                  (unsigned long long) args.gid2,
                  strsignal((int) abort_signal));
        }
    }

    return result;
}
