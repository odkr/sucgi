/*
 * Mock-ups of user and group ID functions
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _BSD_SOURCE
/* _DARWIN_C_SOURCE must NOT be set, or else getgroups won't be interposed */
#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <sys/types.h>
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../../attr.h"
#include "../../macros.h"
#include "../../params.h"
#include "mockstd.h"


/*
 * Interposition
 */

#if defined(__MACH__) && __MACH__ &&                            \
    defined(__GNUC__) &&                                        \
    (__GNUC__ > 3 || (__GNUC__ >= 3 && __GNUC_MINOR__ >= 1)) && \
    !defined(__ICC)

#include <unistd.h>

/* NOLINTBEGIN(performance-no-int-to-ptr) */

/* cppcheck-suppress [misra-c2012-20.10, misra-c2012-20.12]; ## is needed */
#define DYLD_INTERPOSE(_replacement, _replacee)                 \
    __attribute__((used)) static struct {                       \
        const void *replacment;                                 \
        const void *replacee;                                   \
    } _interpose_ ## _replacee                                  \
    __attribute__((section("__DATA, __interpose"))) = {         \
        (const void *) (unsigned long) &(_replacement),         \
        (const void *) (unsigned long) &(_replacee)             \
    };

/* NOLINTEND(performance-no-int-to-ptr) */

#else
#define DYLD_INTERPOSE(_replacement, _replacee)
#endif


/*
 * Constants
 */

/* The number 10 */
#define BASE10 10


/*
 * Prototypes
 */

/* Check whether $MOCK_DEBUG is set */
_no_sanitize_all _nodiscard
static bool debug(void);

/* Check whether a number falls into the range [INT_MIN + 1, INT_MAX - 1] */
_no_sanitize_all _nodiscard
static bool num_is_valid(long long num);

/* Check whether a number is smaller than SIZE_MAX - 1 */
_no_sanitize_all _nodiscard
static bool unum_is_valid(unsigned long long num);

/* Check whether a number is a valid ID */
_no_sanitize_all _nodiscard
static bool id_is_valid(id_t id);

/* Convert an integer to a string */
_no_sanitize_all _write_only(3, 2) _nonnull(3)
static void num_to_str(long long num, size_t size, char *str);

/* Convert an unsigned integer to a string */
_no_sanitize_all _write_only(3, 2) _nonnull(3)
static void unum_to_str(unsigned long long num, size_t size, char *str);

/* Convert an ID to a string */
_no_sanitize_all _write_only(3, 2) _nonnull(3)
static void id_to_str(id_t id, size_t size, char *str);

/* Convert a string to an integer */
_no_sanitize_all _read_only(1) _nonnull(1) _nodiscard
static long long str_to_num(const char *str);

/* Convert a string to an unsigned integer */
_no_sanitize_all _read_only(1) _nonnull(1) _nodiscard
static unsigned long long str_to_unum(const char *str);

/* Convert a string to an ID */
_no_sanitize_all _read_only(1) _nonnull(1) _nodiscard
static id_t str_to_id(const char *str);

/* Get an ID from an environment variable */
_no_sanitize_all _read_only(1) _nonnull(1) _nodiscard
static id_t get_id_env(const char *name);

/* Get an error number from an environment variable */
_no_sanitize_all _read_only(1) _nonnull(1) _nodiscard
static int get_errno(const char *name);

/* Set an environment variable to an integer value */
_no_sanitize_all _read_only(1) _nonnull(1)
static void set_num_env(const char *name, long long num);

/* Set an environment variable to an unsigned integer value */
_no_sanitize_all _read_only(1) _nonnull(1)
static void set_unum_env(const char *name, unsigned long long num);

/* Set an environment variable to an ID */
_no_sanitize_all _read_only(1) _nonnull(1)
static void set_id_env(const char *name, id_t id);

/* Get the saved-user ID from the environment */
_no_sanitize_all _nodiscard
static uid_t get_suid(void);

/* Get the saved-group ID from the environment */
_no_sanitize_all _nodiscard
static gid_t get_sgid(void);


/*
 * Functions
 */

static bool
debug(void)
{
    errno = 0;
    /* RATS: ignore; used for debugging */
    const char *const var = getenv("MOCK_DEBUG");
    if (var == NULL) {
        /* cppcheck-suppress misra-c2012-22.10; getenv sets errno */
        if (errno == 0) {
            return false;
        }
        err(EXIT_FAILURE, "%s:%d: getenv MOCK_DEBUG", __FILE__, __LINE__);
    }

    return *var != '\0';
}

static bool
num_is_valid(const long long num) {
    return ((int) INT_MIN + 1) < num && num < ((int) INT_MAX - 1);
}

static bool
unum_is_valid(const unsigned long long num) {
    return (uintmax_t) num < ((uintmax_t) UINT_MAX - 1U);
}

static bool
id_is_valid(const id_t id) {
    /* cppcheck-suppress cert-INT31-c; cast is safe */
    return ISSIGNED(id_t) ?
        num_is_valid((long long) id) :
        unum_is_valid((unsigned long long) id);
}

static void
num_to_str(const long long num, const size_t size, char *const str)
{
    assert(size > 0U);
    assert(str != NULL);

    (void) memset(str, '\0', size);

    errno = 0;
    /* RATS: ignore; size is correctly given by all callees */
    int len = snprintf(str, size, "%lld", num);

    if (len < 0) {
        err(EXIT_FAILURE, "%s:%d: snprintf", __FILE__, __LINE__);
    }

    if ((size_t) len >= size) {
        errx(EXIT_FAILURE, "%s:%d: %lld has > %zu chars in base-10",
             __FILE__, __LINE__, num, size - 1U);
    }

    assert(str[len] == '\0');
}

static void
unum_to_str(const unsigned long long num, const size_t size, char *const str)
{
    assert(size > 0U);
    assert(str != NULL);

    (void) memset(str, '\0', size);

    errno = 0;
    /* RATS: ignore; size is correctly given by all callers */
    int len = snprintf(str, size, "%llu", num);

    if (len < 0) {
        err(EXIT_FAILURE, "%s:%d: snprintf", __FILE__, __LINE__);
    }

    if ((size_t) len >= size) {
        errx(EXIT_FAILURE, "%s:%d: %llu has > %zu digits in base-10",
             __FILE__, __LINE__, num, size - 1U);
    }

    assert(str[len] == '\0');
}

static void
id_to_str(const id_t id, const size_t size, char *const str)
{
    assert(size > 0U);
    assert(str != NULL);

    /* cppcheck-suppress cert-INT31-c; cast is safe */
    if (ISSIGNED(id_t)) {
        num_to_str((long long) id, size, str);
    } else {
        unum_to_str((unsigned long long) id, size, str);
    }
}

static long long
str_to_num(const char *const str)
{
    assert(str != NULL);
    assert(*str != '\0');

    errno = 0;
    long long num = strtoll(str, NULL, BASE10);
    /* cppcheck-suppress misra-c2012-22.10; strtoll may sets errno */
    if (num == (long long) 0 && errno != 0) {
        err(EXIT_FAILURE, "%s:%d: strtoll", __FILE__, __LINE__);
    }

    if (!num_is_valid(num)) {
        errx(EXIT_FAILURE, "%s:%d: %lld: not a valid ID",
             __FILE__, __LINE__, num);
    }

    return num;
}

static unsigned long long
str_to_unum(const char *const str)
{
    assert(str != NULL);
    assert(*str != '\0');

    errno = 0;
    unsigned long long num = strtoull(str, NULL, BASE10);
    if (num == 0U && errno != 0) {
        err(EXIT_FAILURE, "%s:%d: strtoull", __FILE__, __LINE__);
    }

    if (!unum_is_valid(num)) {
        errx(EXIT_FAILURE, "%s:%d: %llu: not a valid ID",
             __FILE__, __LINE__, num);
    }

    return num;
}

static id_t
str_to_id(const char *const str)
{
    assert(str != NULL);
    assert(*str != '\0');

    /* cppcheck-suppress cert-INT31-c; cast is safe */
    return  ISSIGNED(id_t) ?
        (id_t) str_to_num(str) :
        (id_t) str_to_unum(str);
}

static id_t
get_id_env(const char *const name)
{
    assert(name != NULL);
    assert(*name != '\0');
    assert(strchr(name, '=') == NULL);

    errno = 0;
    /* RATS: ignore; used for debugging */
    const char *const value = getenv(name);
    if (value == NULL) {
        /* cppcheck-suppress misra-c2012-22.10; getenv sets errno */
        if (errno == 0) {
            return (id_t) 0;
        }

        err(EXIT_FAILURE, "%s:%d: getenv %s", __FILE__, __LINE__, name);
    }

    const id_t id = str_to_id(value);
    if (debug()) {
	/* cppcheck-suppress cert-INT31-c; cast is safe */
        if (ISSIGNED(id_t)) {
            warnx("get %s=%lld", name, (long long) id);
        } else {
            warnx("get %s=%llu", name, (unsigned long long) id);
        }
    }

    return id;
}

static void
set_num_env(const char *const name, const long long num)
{
    assert(name != NULL);
    assert(*name != '\0');
    assert(strchr(name, '=') == NULL);

    /* RATS: ignore; num_to_str is bounded by sizeof(value) */
    char value[MAX_STR_LEN];
    num_to_str(num, sizeof(value), value);

    errno = 0;
    if (setenv(name, value, true) != 0) {
        err(EXIT_FAILURE, "%s:%d: setenv %s=%s",
            __FILE__, __LINE__, name, value);
    }
}

static void
set_unum_env(const char *const name, const unsigned long long num)
{
    assert(name != NULL);
    assert(*name != '\0');
    assert(strchr(name, '=') == NULL);

    /* RATS: ignore; unum_to_str is bounded by sizeof(value) */
    char value[MAX_STR_LEN];
    unum_to_str(num, sizeof(value), value);

    errno = 0;
    if (setenv(name, value, true) != 0) {
        err(EXIT_FAILURE, "%s:%d: setenv %s=%s",
            __FILE__, __LINE__, name, value);
    }
}

static void
set_id_env(const char *const name, const id_t id)
{
    assert(name != NULL);
    assert(*name != '\0');
    assert(strchr(name, '=') == NULL);

    /* cppcheck-suppress cert-INT31-c; cast is safe */
    if (ISSIGNED(id_t)) {
        set_num_env(name, (long long) id);

        if (debug()) {
            warnx("set %s=%lld", name, (long long) id);
        }
    } else {
        set_unum_env(name, (unsigned long long) id);

        if (debug()) {
            warnx("set %s=%llu", name, (unsigned long long) id);
        }
    }
}

static uid_t
get_suid(void)
{
    return (uid_t) get_id_env("MOCK_SUID");
}

static gid_t
get_sgid(void)
{
    return (gid_t) get_id_env("MOCK_SGID");
}

static int
get_errno(const char *const name)
{
    assert(name != NULL);
    assert(*name != '\0');

    errno = 0;
    /* RATS: ignore; used for debugging */
    const char *const value = getenv(name);
    if (value == NULL) {
        /* cppcheck-suppress misra-c2012-22.10; getenv sets errno */
        if (errno == 0) {
            return 0;
        }

        err(EXIT_FAILURE, "%s:%d: getenv %s", __FILE__, __LINE__, name);
    }

    errno = 0;
    long long num = strtoll(value, NULL, BASE10);

    /* cppcheck-suppress misra-c2012-22.10; strtoll may sets errno */
    if (num == (long long) 0 && errno != 0) {
        err(EXIT_FAILURE, "%s:%d: strtoll %s", __FILE__, __LINE__, value);
    }

    if (num < INT_MIN) {
        err(EXIT_FAILURE, "%s:%d: %lld is smaller than %d",
            __FILE__, __LINE__, num, (int) INT_MIN);
    }

    if (num > INT_MAX) {
        err(EXIT_FAILURE, "%s:%d: %lld is greater than %d",
            __FILE__, __LINE__, num, (int) INT_MAX);
    }

    if (debug()) {
        warnx("%s:%d: get %s=%lld", __FILE__, __LINE__, name, num);
    }

    return (int) num;
}

uid_t
mock_getuid(void)
{
    return (uid_t) get_id_env("MOCK_UID");
}
DYLD_INTERPOSE(mock_getuid, getuid)

uid_t
mock_geteuid(void)
{
    return (uid_t) get_id_env("MOCK_EUID");
}
DYLD_INTERPOSE(mock_geteuid, geteuid)

int
mock_setuid(const uid_t uid)
{
    int mockerr = get_errno("MOCK_SETUID_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    if (!id_is_valid(uid)) {
        errno = EINVAL;
        return -1;
    }

    uid_t euid = mock_geteuid();
    if ((uintmax_t) euid != (uintmax_t) 0 &&
	(uintmax_t) get_suid() != (uintmax_t) 0 &&
	uid != euid &&
	uid != mock_getuid())
    {
        errno = EPERM;
        return -1;
    }

    set_id_env("MOCK_UID", uid);
    set_id_env("MOCK_EUID", uid);
    set_id_env("MOCK_SUID", uid);

    return 0;
}
DYLD_INTERPOSE(mock_setuid, setuid)

int
mock_seteuid(const uid_t euid)
{
    int mockerr = get_errno("MOCK_SETEUID_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    if (!id_is_valid(euid)) {
        errno = EINVAL;
        return -1;
    }

    if ((uintmax_t) mock_geteuid() != (uintmax_t) 0 &&
	euid != mock_getuid() &&
	euid != get_suid())
    {
        errno = EPERM;
        return -1;
    }

    set_id_env("MOCK_EUID", euid);

    return 0;
}
DYLD_INTERPOSE(mock_seteuid, seteuid)

/*
 * setreuid and setregid accept -1 as ID. So -1 is a valid, if weird, ID.
 * But POSIX.1-2008 allows for uid_t, gid_t, and id_t to be defined as
 * unsigned integers, and that is how they are defined on most systems.
 * So IDs must be compared against -1 even if uid_t, gid_t, and id_t
 * are unsigned; in other words, the sign change is intentional.
 */
#if defined(__GNUC__) && (__GNUC__ > 2 || \
    (__GNUC__ >= 2 && __GNUC_MINOR__ >= 95))
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif

int
mock_setreuid(const uid_t uid, const uid_t euid)
{
    uid_t currentuid = mock_getuid();
    uid_t currenteuid = mock_geteuid();

    int mockerr = get_errno("MOCK_SETREUID_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    /* cppcheck-suppress cert-INT31-c; -1 is a special UID value */
    if (uid != (uid_t) -1 && uid != currentuid) {
        if (!id_is_valid(uid)) {
            errno = EINVAL;
            return -1;
        }

        if (currenteuid != 0 && uid != currenteuid) {
            errno = EPERM;
            return -1;
        }

        set_id_env("MOCK_UID", uid);
    }

    /* cppcheck-suppress cert-INT31-c; -1 is a special UID value */
    if (euid != (uid_t) -1 && euid != currenteuid) {
        if (!id_is_valid(euid)) {
            errno = EINVAL;
            return -1;
        }

        if ((uintmax_t) currenteuid != (uintmax_t) 0 &&
	    euid != currentuid)
	{
            errno = EPERM;
            return -1;
        }

        set_id_env("MOCK_EUID", euid);
    }

    return 0;
}
DYLD_INTERPOSE(mock_setreuid, setreuid)

#if defined(__GNUC__) && (__GNUC__ > 2 || \
    (__GNUC__ >= 2 && __GNUC_MINOR__ >= 95))
#pragma GCC diagnostic pop
#endif

gid_t
mock_getgid(void)
{
    return (gid_t) get_id_env("MOCK_GID");
}
DYLD_INTERPOSE(mock_getgid, getgid)

gid_t
mock_getegid(void)
{
    return (gid_t) get_id_env("MOCK_EGID");
}
DYLD_INTERPOSE(mock_getegid, getegid)

int
mock_setgid(const gid_t gid)
{
    int mockerr = get_errno("MOCK_SETGID_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    if (!id_is_valid(gid)) {
        errno = EINVAL;
        return -1;
    }

    if ((uintmax_t) mock_geteuid() != (uintmax_t) 0 &&
	(uintmax_t) get_suid() != (uintmax_t) 0 &&
        gid != mock_getgid() &&
	gid != mock_getegid())
    {
        errno = EPERM;
        return -1;
    }

    set_id_env("MOCK_GID", gid);
    set_id_env("MOCK_EGID", gid);
    set_id_env("MOCK_SGID", gid);

    return 0;
}
DYLD_INTERPOSE(mock_setgid, setgid)

int
mock_setegid(const gid_t egid)
{
    int mockerr = get_errno("MOCK_SETEGID_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    if (!id_is_valid(egid)) {
        errno = EINVAL;
        return -1;
    }

    if ((uintmax_t) mock_getegid() != (uintmax_t) 0 &&
	egid != mock_getgid() &&
	egid != get_sgid())
    {
        errno = EPERM;
        return -1;
    }

    set_id_env("MOCK_EGID", egid);

    return 0;
}
DYLD_INTERPOSE(mock_setegid, setegid)

/* See above */
#if defined(__GNUC__) && (__GNUC__ > 2 || \
    (__GNUC__ >= 2 && __GNUC_MINOR__ >= 95))
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif

int
mock_setregid(const gid_t gid, const gid_t egid)
{
    uid_t currenteuid = mock_geteuid();
    gid_t currentgid = mock_getgid();
    gid_t currentegid = mock_getegid();

    int mockerr = get_errno("MOCK_SETREGID_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    /* cppcheck-suppress cert-INT31-c; -1 is a special GID value */
    if (gid != (gid_t) -1 && gid != currentgid) {
        if (!id_is_valid(gid)) {
            errno = EINVAL;
            return -1;
        }

        if (currenteuid != 0 && gid != currentegid) {
            errno = EPERM;
            return -1;
        }

        set_id_env("MOCK_GID", gid);
    }

    /* cppcheck-suppress cert-INT31-c; -1 is special GID value */
    if (egid != (gid_t) -1 && egid != currentegid) {
        if (!id_is_valid(egid)) {
            errno = EINVAL;
            return -1;
        }

        if ((uintmax_t) currenteuid != (uintmax_t) 0) {
            errno = EPERM;
            return -1;
        }

        set_id_env("MOCK_EGID", egid);
    }

    return 0;
}
DYLD_INTERPOSE(mock_setregid, setregid)

#if defined(__GNUC__) && (__GNUC__ > 2 || \
    (__GNUC__ >= 2 && __GNUC_MINOR__ >= 95))
#pragma GCC diagnostic pop
#endif

int
mock_getgroups(int gidsetlen, gid_t *gidset)
{
    errno = 0;
    /* RATS: ignore; used for debugging */
    const char *envgroups = getenv("MOCK_GROUPS");
    if (envgroups == NULL) {
        /* cppcheck-suppress misra-c2012-22.10; getenv sets errno */
        if (errno == 0) {
            if (debug()) {
                warnx("get MOCK_GROUPS=");
            }
            return 0;
        }
        err(EXIT_FAILURE, "%s:%d: getenv MOCK_GROUPS", __FILE__, __LINE__);
    }

    if (debug()) {
        warnx("get MOCK_GROUPS=%s", envgroups);
    }

    /* RATS: ignore; stpncpy is bounded by sizeof(groups) - 1U */
    char groups[MAX_STR_LEN];
    const char *end = stpncpy(groups, envgroups, sizeof(groups) - 1U);

    assert(end != NULL);
    assert(end >= groups);
    assert(*end == '\0');

    assert(end >= groups);
    /* cppcheck-suppress misra-c2012-18.4; safe pointer subtraction */
    ptrdiff_t nbytes = end - groups;

    if (envgroups[nbytes] != '\0') {
        err(EXIT_FAILURE, "%s:%d: MOCK_GROUPS is too long",
            __FILE__, __LINE__);
    }

    int ngroups = 0;
    const char *group = groups;
    do {
        if (ngroups >= gidsetlen) {
            return -1;
        }

        char *sep = strchr(groups, ',');
        if (sep != NULL) {
            *sep = '\0';
        }

        gidset[ngroups] = (gid_t) str_to_id(group);
        ++ngroups;

        if (sep == NULL) {
            break;
        }

        group = &sep[1];
    } while (true);

    return ngroups;
}
DYLD_INTERPOSE(mock_getgroups, getgroups)

int
mock_setgroups(const NGRPS_T ngroups, const gid_t *const gidset)
{
    assert(gidset != NULL);

    /* RATS: ignore; stpncpy is bounded by lim */
    char str[MAX_STR_LEN];
    (void) memset(str, '\0', sizeof(str));

    int mockerr = get_errno("MOCK_SETGROUPS_ERRNO");
    if (mockerr != 0) {
        errno = mockerr;
        return -1;
    }

    if (mock_geteuid() != 0) {
        errno = EPERM;
        return -1;
    }

    if ((size_t) ngroups > (size_t) MAX_NGROUPS) {
        errno = EINVAL;
        return -1;
    }

    const char *lim = &str[sizeof(str) - 1U];
    char *ptr = str;
    /* cppcheck-suppress misra-c2012-14.2; for-loop is well-formed */
    for (NGRPS_T i = 0; i < ngroups; ++i) {
        if (ptr > str) {
            *ptr = ',';
            ++ptr;
        }

        /* RATS: ignore; id_to_str is bounded by sizeof(group) */
        char group[MAX_STR_LEN];
        id_to_str(gidset[i], sizeof(group), group);

        assert(lim >= ptr);
        /* cppcheck-suppress misra-c2012-18.4; safe pointer subtraction */
        ptrdiff_t len = lim - ptr;
        assert((uintmax_t) len <= SIZE_MAX);

        ptr = stpncpy(ptr, group, (size_t) len);

        assert(ptr >= str);
        /* cppcheck-suppress misra-c2012-18.4; safe pointer subtraction */
        ptrdiff_t end = ptr - str;

        if (group[end] != '\0') {
            err(EXIT_FAILURE, "%s:%d: too many GIDs", __FILE__, __LINE__);
        }
    }

    if (setenv("MOCK_GROUPS", str, true) != 0) {
        err(EXIT_FAILURE, "%s:%d: setenv MOCK_GROUPS=%s",
            __FILE__, __LINE__, str);
    }

    if (debug()) {
        warnx("set MOCK_GROUPS=%s", str);
    }

    return 0;
}
DYLD_INTERPOSE(mock_setgroups, setgroups)
