/*
 * Test path_get_reall
 *
 * Copyright 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 700

#include <sys/stat.h>
#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <setjmp.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../attr.h"
#include "../macros.h"
#include "../params.h"
#include "../path.h"
#include "../types.h"
#include "types.h"
#include "libutil/abort.h"
#include "libutil/array.h"
#include "libutil/dir.h"
#include "libutil/path.h"
#include "libutil/sigs.h"
#include "libutil/tmpdir.h"
#include "libutil/types.h"


/*
 * Macros
 */

/* Maximum number of return values to check for */
#define MAX_NRETVALS 3


/*
 * Data types
 */

/* Types of files to test path_get_real with */
typedef enum {
    FT_FILE,                            /* Existing file */
    FT_NOFILE,                          /* Non-existing file */
    FT_DIR,                             /* Directory */
    FT_NODIR,                           /* Non-existing directory */
    FT_LINK_FILE,                       /* Link to file */
    FT_LINK_NOFILE,                     /* Link to non-existing file */
    FT_NOLINK                           /* Non-existing link */
} FileType;

/* Mapping of filenames to return values */
typedef struct {
    const FileType type;                /* Filetype */
    const char *const fname;            /* Filename */
    const char *const real;             /* Real name of that file */
    const size_t nretvals;              /* Number of legal return values */
    const Error retvals[MAX_NRETVALS];  /* Legal return values */
    /* cppcheck-suppress cert-API01-C */
    int signal;                         /* Expected signal */
} PathRealArgs;


/*
 * Module variables
 */

/* Path of temporary directory */
static char *tmpdir = NULL;

/* Last signal caught by catch_sig */
static volatile sig_atomic_t caught = 0;


/*
 * Prototypes
 */

/*
 * Create the given file and every directory along the way.
 * "errh" is an error handler to pass to path_walk.
 *
 * Return value:
 *     Zero      Success.
 *     Non-zero  An error occurred; errno should be set.
 */
_read_only(1) _nonnull(1)
static int make_file(const char *fname, ErrorFn errh);

/*
 * Create the given directory and every directory along the way.
 * "errh" is an error handler to pass to path_walk.
 *
 * Return value:
 *     Zero      Success.
 *     Non-zero  An error occurred; errno should be set.
 */
_read_only(1) _nonnull(1)
static int make_path(const char *fname, ErrorFn errh);

/*
 * Create a link to the given file and every directory along the way.
 * "errh" is an error handler to pass to path_walk.
 *
 * Return value:
 *     Zero      Success.
 *     Non-zero  An error occurred; errno should be set.
 */
_read_only(1) _nonnull(1)
static int make_link(const char *fname, const char *link, ErrorFn errh);

/*
 * Wrapper around open/close to pass to path_walk.
 */
_read_only(1) _nonnull(1) _nodiscard
static int make_file_w(const char *fname, size_t nargs, va_list argp);

/*
 * Wrapper around mkdir to pass to path_walk.
 */
_read_only(1) _nonnull(1) _nodiscard
static int make_path_w(const char *fname, size_t nargs, va_list argp);

/*
 * Wrapper around symlink to pass to path_walk.
 */
_read_only(1) _nonnull(1) _nodiscard
static int make_link_w(const char *fname, size_t nargs, va_list argp);

/*
 * Remove the given file or directory, recursively if necessary.
 * Does not trigger an error if the file does not exist.
 * Otherwise the same as dir_tree_rm.
 *
 * Return value:
 *     See dir_tree_rm.
 */
_read_only(1) _nonnull(1)
static int rm_dir_tree_silently(const char *fname, ErrorFn errh);

/*
 * Check whether two errors are equal.
 *
 * Return value:
 *     Zero      Errors are equal.
 *     Non-zero  Otherwise.
 */
_read_only(1) _read_only(2) _nonnull(1, 2) _nodiscard
static int cmp_errs(const Error *err1, const Error *err2);

/*
 * Store the given signal in the global "caught".
 */
static void catch_sig(int signal);

/*
 * Remove the temporary directory unless the global "tmpdir" is NULL.
 */
static void cleanup(void);


/*
 * Main
 */

int
main(void)
{
    (void) sigs_handle(catch_sig, SIGS_NTERM, sigs_term, NULL, err);

    /* RATS: ignore; umask is restrictive */
    (void) umask(S_ISUID | S_ISGID | S_ISVTX | S_IRWXG | S_IRWXO);

    errno = 0;
    if (atexit(cleanup) != 0) {
        err(ERROR, "atexit");
    }

    (void) tmpdir_make("tmp-XXXXXX", &tmpdir, err);
    assert(tmpdir != NULL);

    const size_t tmpdirlen = strnlen(tmpdir, MAX_FNAME_LEN);

    if (tmpdirlen >= (size_t) MAX_FNAME_LEN) {
        errx(ERROR, "%s: filename too long", tmpdir);
    }

    if (chdir(tmpdir) != 0) {
        err(ERROR, "chdir %s", tmpdir);
    }

    /* RATS: ignore; used safely */
    char hugefname[MAX_FNAME_LEN + 1];
    path_gen(sizeof(hugefname), hugefname);

    /* RATS: ignore; used safely */
    char longfname[MAX_FNAME_LEN];
    
    /* "- 2U" because of '/' and musl */
    path_gen(sizeof(longfname) - tmpdirlen - 2U, longfname);

    /* cppcheck-suppress [misra-c2012-9.3, misra-c2012-9.4] */
    const PathRealArgs cases[] = {
#if !defined(NDEBUG)
        /* Bad arguments */
        {FT_NOFILE, "", "<n/a>", 1, {OK}, SIGABRT},
#endif

        /* Filename is too long before resolution */
        {FT_NOFILE, hugefname, "<n/a>", 1, {ERR_LEN, ERR_SYS}, 0},
        {FT_FILE, hugefname, "<n/a>", 1, {ERR_LEN, ERR_SYS}, 0},

        /* Long filename */
        {FT_NOFILE, longfname, "<n/a>", 1, {ERR_SYS}, 0},
        /* Fails on Linux with ENOENT */
        {FT_FILE, longfname, longfname, 1, {OK}, 0},

        /* Simple tests */
        {FT_FILE, "file", "file", 1, {OK}, 0},
        {FT_NOFILE, "<nosuchfile>", NULL, 1, {ERR_SYS}, 0},
        {FT_DIR, "dir", "dir", 1, {OK}, 0},
        {FT_NODIR, "<nosuchdir>", NULL, 1, {ERR_SYS}, 0},
        {FT_LINK_FILE, "symlink", "linked", 1, {OK}, 0},
        {FT_LINK_NOFILE, "<nosuchlink>", "<n/a>", 1, {ERR_SYS}, 0},
        {FT_LINK_NOFILE, "linktonowhere", "<nosuchfile>", 1, {ERR_SYS}, 0},
        {FT_NOLINK, "<nosuchlink>", "<nosuchfile>", 1, {ERR_SYS}, 0},
        {FT_FILE, "dir/file", "dir/file", 1, {OK}, 0},
        {FT_NOFILE, "dir/<nosuchfile>", "<n/a>", 1, {ERR_SYS}, 0},
        {FT_DIR, "dir/subdir", "dir/subdir", 1, {OK}, 0},
        {FT_NODIR, "dir/<nosuchdir>", "<n/a>", 1, {ERR_SYS}, 0},
        {FT_LINK_FILE, "dir/symlink", "dir/linked", 1, {OK}, 0},
        {FT_NOLINK, "dir/<nosuchlink>", "<n/a>", 1, {ERR_SYS}, 0},
        {FT_LINK_NOFILE, "linktonowhere", "<nosuchfile>", 1, {ERR_SYS}, 0},
        {FT_NOLINK, "<nosuchlink>", "<nosuchfile>", 1, {ERR_SYS}, 0},

        /* Links to filenames that are too long */
        {FT_LINK_NOFILE, "hugelink", hugefname, 1, {ERR_SYS}, 0},
        {FT_LINK_FILE, "hugelink", hugefname, 1, {ERR_SYS}, 0}
    };

    volatile int result = PASS;

    for (volatile size_t i = 0; i < NELEMS(cases) && caught == 0; ++i) {
        const PathRealArgs args = cases[i];

        if (sigsetjmp(abort_env, 1) == 0) {
            size_t fnamelen = strnlen(args.fname, MAX_STR_LEN);
            assert(fnamelen < MAX_STR_LEN);

            switch(args.type) {
            case FT_LINK_FILE:
                errno = 0;
                (void) make_link(args.real, args.fname, err);
                /* cppcheck-suppress misra-c2012-16.3; falls through */
            case FT_FILE:
                errno = 0;
                (void) make_file(args.real, err);
                break;
            case FT_DIR:
                errno = 0;
                (void) make_path(args.real, err);
                break;
            default:
                break;
            }

            if (args.signal != 0) {
                warnx("the next test should fail an assertion.");
            }

            char *real = NULL;
            size_t reallen = 0;

            (void) abort_catch(err);
            const Error retval = path_get_real(fnamelen, args.fname,
                                               &reallen, &real);
            (void) abort_reset(err);

            /* cppcheck-suppress misra-c2012-11.5; this conversion is fine */
            const Error *const retvalp = array_find(
                &retval, args.retvals, args.nretvals,
                sizeof(*args.retvals), (CompFn) cmp_errs
            );

            if (retvalp == NULL) {
                result = FAIL;
                warnx("(%zu, %s, -> %zu, -> %s) -> %u [!]",
                      fnamelen, args.fname, reallen, real, retval);
            }

            if (retval == OK) {
                if (strnlen(real, MAX_FNAME_LEN) != reallen ||
                    reallen >= (size_t) MAX_FNAME_LEN)
                {
                    result = FAIL;
                    warnx("(%zu), %s, -> %zu [!], -> %s) -> %u",
                          fnamelen, args.fname, reallen, real, retval);
                }

                if (strcmp(args.real, &real[tmpdirlen + 1U]) != 0) {
                    result = FAIL;
                    warnx("(%zu, %s, -> %zu, -> %s [!]) -> %u",
                          fnamelen, args.fname, reallen, real, retval);
                }
            }

            if (real != NULL) {
                free(real);
            }
        }

        if (abort_signal != args.signal) {
            result = FAIL;
            warnx("(<fnamelen>, %s, -> <reallen>, -> <real>) ^ %s [!]",
                  args.fname, strsignal((int) abort_signal));
        }

        char *root = NULL;
        switch(args.type) {
        case FT_LINK_FILE:
            root = path_split(args.real, NULL, err);
            (void) rm_dir_tree_silently(root, err);
            free(root);
            /* cppcheck-suppress misra-c2012-16.3; falls through */
        case FT_FILE:
            /* cppcheck-suppress misra-c2012-16.3; falls through */
        case FT_DIR:
            root = path_split(args.fname, NULL, err);
            (void) rm_dir_tree_silently(root, err);
            free(root);
            break;
        default:
            break;
        }
    }

    if (caught != 0) {
        warnx("%s", strsignal((int) caught));
        cleanup();
        (void) sigs_reraise((int) caught, err);
    }

    return result;
}


/*
 * Functions
 */

static int
make_file(const char *const fname, const ErrorFn errh)
{
    int retval = 0;

    assert(fname != NULL);
    assert(*fname != '\0');

    errno = 0;
    retval = path_walk(fname, make_path_w, make_file_w, NULL, 0);
    if (retval != 0 && errh != NULL) {
        errh(ERROR, "make_file %s", fname);
    }

    return retval;
}

static int
make_path(const char *const fname, const ErrorFn errh)
{
    int retval = 0;

    assert(fname != NULL);
    assert(*fname != '\0');

    errno = 0;
    retval = path_walk(fname, make_path_w, make_path_w, NULL, 0);
    if (retval != 0 && errh != NULL) {
        errh(ERROR, "make_path %s", fname);
    }

    return retval;
}

static int
make_link(const char *const fname, const char *const link, const ErrorFn errh)
{
    const char *abslink;
    /* RATS: ignore; used safely */
    char buffer[MAX_FNAME_LEN] = {0};
    int retval = 0;

    assert(fname != NULL);
    assert(*fname != '\0');
    assert(link != NULL);
    assert(*link != '\0');

    if (*link == '/') {
        abslink = link;
    } else {
        /* RATS: ignore; used safely */
        char curwd[MAX_FNAME_LEN] = {0};

        errno = 0;
        if (getcwd(curwd, sizeof(curwd)) == NULL) {
            if (errh != NULL) {
                errh(ERROR, "make_link %s %s", fname, link);
            }
            return -1;
        }

        errno = 0;
        retval = path_join(sizeof(buffer), buffer, curwd, link, errh);
        if (retval < 0) {
            if (errh != NULL) {
                errh(ERROR, "make_link %s %s", fname, link);
            }
            return -1;
        }

        abslink = buffer;
    }

    errno = 0;
    retval = path_walk(fname, make_path_w, make_link_w, NULL, 1, abslink);
    if (retval != 0 && errh != NULL) {
        errh(ERROR, "make_link %s %s", fname, link);
    }

    return retval;
}

/* NOLINTBEGIN(misc-unused-parameters); Clang-Tidy doesn't get _unused */

static int
make_file_w(const char *const fname,
            const size_t nargs _unused, va_list argp _unused)
{
    int fildes = -1;

    assert(fname != NULL);
    assert(*fname != '\0');

    errno = 0;
    /* RATS: ignore; no race */
    fildes = open(fname, O_RDONLY | O_CREAT | O_EXCL | O_CLOEXEC, S_IRWXU);
    if (fildes < 0) {
        return -1;
    }

    return sigs_retry_i(close, fildes);
}

static int
make_path_w(const char *const fname,
            const size_t nargs _unused, va_list argp _unused)
{
    int retval = 0;

    assert(fname != NULL);
    assert(*fname != '\0');

    errno = 0;
    /* RATS: ignore; no race condition */
    retval = mkdir(fname, S_IRWXU);
    /* cppcheck-suppress misra-c2012-22.10; mkdir sets errno */
    if (retval != 0 && errno != EEXIST) {
        return retval;
    }

    return 0;
}

/* NOLINTEND(misc-unused-parameters) */

static int
make_link_w(const char *const fname, const size_t nargs, va_list argp)
{
    assert(fname != NULL);
    assert(*fname != '\0');
    assert(nargs == 1U);

    errno = 0;
    return symlink(fname, (const char *) va_arg(argp, const char *));
}

static int
rm_dir_tree_silently(const char *const fname, const ErrorFn errh)
{
    int retval = 0;

    errno = 0;
    retval = dir_tree_rm(fname, NULL);
    /* cppcheck-suppress misra-c2012-22.10; mkdir sets errno */
    if (retval != 0 && errno != ENOENT) {
        errh(ERROR, "dir_tree_rm %s", fname);
    }

    return retval;
}

static int
cmp_errs(const Error *const err1, const Error *const err2)
{
    if (*err1 < *err2) {
        return -1;
    }

    if (*err1 > *err2) {
        return 1;
    }

    return 0;
}

static void
catch_sig(const int signal)
{
    caught = signal;
}

static void
cleanup(void)
{
    if (tmpdir != NULL) {
        if (dir_tree_rm(tmpdir, NULL) != 0) {
            warn("dir_tree_rm %s", tmpdir);
        }
    }
}
