/*
 * Run a command as a given user
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _BSD_SOURCE
#define _DARWIN_C_SOURCE
#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <sys/types.h>
#include <err.h>
#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>


/*
 * Constants
 */

/* Exit status for usage errors */
#define EXIT_USAGE 2


/*
 * Main
 */

int
main (int argc, char **argv)
{
    int opt;
    /* RATS: ignore; runas is not security-critical */
    while ((opt = getopt(argc, argv, "h")) != -1) {
        switch (opt) {
        case 'h':
            (void) puts(
"runas - run a command as a given user\n\n"
"Usage:       runas logname cmd [arg ...]\n"
"             runas -h\n\n"
"Operands:\n"
"    logname  Login name.\n"
"    cmd      Command to run.\n"
"    arg      Argument to cmd.\n\n"
"Options:\n"
"    -h       Print this help screen.\n\n"
"Copyright 2022 and 2023 Odin Kroeger.\n"
"Released under the GNU Affero General Public License.\n"
"This programme comes with ABSOLUTELY NO WARRANTY."
            );
            return EXIT_SUCCESS;
        default:
            return EXIT_USAGE;
        }
    }

    argc -= optind; 
    argv += optind;     /* cppcheck-suppress misra-c2012-18.4 */

    if (argc < 2) {
        (void) fputs("usage: runas logname cmd [arg ...]\n", stderr);
        return EXIT_USAGE;
    }

    errno = 0;
    /* cppcheck-suppress getpwnamCalled; runas is not threaded. */
    const struct passwd *const pwd = getpwnam(argv[0]);
    if (!pwd) {
        if (errno == 0) {
            errx(EXIT_FAILURE, "no such user");
        } else {
            err(EXIT_FAILURE, "getpwnam");
        }
    }

    if (setgroups(1, (gid_t[1]) {(gid_t) pwd->pw_gid}) != 0) {
        err(EXIT_FAILURE, "setgroups");
    }

    if (setgid(pwd->pw_gid) != 0) {
        err(EXIT_FAILURE, "setgid");
    }

    if (setuid(pwd->pw_uid) != 0) {
        err(EXIT_FAILURE, "setuid");
    }

    char **comm = &argv[1];
    /* RATS: ignore; safe enough */
    (void) execvp(*comm, comm);

    err(EXIT_FAILURE, "exec %s", *comm);
}
