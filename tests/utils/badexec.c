/*
 * Run a programme with a custom/without argv[0]
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#define _BSD_SOURCE
#define _DARWIN_C_SOURCE
#define _DEFAULT_SOURCE
#define _GNU_SOURCE

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>


/*
 * Constants
 */

/* Exit status for usage errors */
#define EXIT_USAGE 2


/*
 * Main
 */

int
main (int argc, char **argv)
{
    int opt;
    /* RATS: ignore; badexec is not security-critical */
    while ((opt = getopt(argc, argv, "h")) != -1) {
        switch (opt) {
        case 'h':
            (void) puts(
"badexec - run a programme with a custom/without argv[0]\n\n"
"Usage:    badexec cmd [arg ...]\n"
"          badexec -h\n\n"
"Operands:\n"
"    cmd  Command to run.\n"
"    arg  Argument to cmd, starting with the 0th argument\n\n"
"Options:\n"
"    -h    Print this help screen.\n\n"
"Copyright 2023 Odin Kroeger.\n"
"Released under the GNU Affero General Public License.\n"
"This programme comes with ABSOLUTELY NO WARRANTY."
            );
            return EXIT_SUCCESS;
        default:
            return EXIT_USAGE;
        }
    }

    argc -= optind; 
    argv += optind;     /* cppcheck-suppress misra-c2012-18.4 */

    if (argc < 1) {
        (void) fputs("usage: badexec cmd [arg ...]\n", stderr);
        return 2;
    }

    errno = 0;
    /* RATS: ignore; safe enough */
    (void) execvp(argv[0], &argv[1]);
    err(EXIT_FAILURE, "exec %s", argv[0]);
}
