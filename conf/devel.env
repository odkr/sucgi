# suCGI build configuration for development.
# Use "addcflags [COMPILER ...] FLAG [...]" to add compiler flags.
# Use "addcppflags FLAG [...]" to add C preprocessor flags.
# Use "addldflags [COMPILER ...] FLAG [...]" to add linker flags.

# Use ISO/IEC 9899:1999 C. Some compilers require this flag.
addcflags -std=c99 -pedantic

# Optimise the build. Required for instrumentation.
addcflags -O1 -Og

# Make code position-independent.
addldflags -pie
addcflags -fPIC -fPIE

# Enforce __attribute__((nonnull(...))).
addcflags -fisolate-erroneous-paths-attribute

# Make sure calculations and conversions don't over- or underflow.
addcflags -ftrapv -fstrict-overflow \
          -fp-trap=invalid,overflow,underflow,denormal \
          -fsanitize=integer

# Enable link time optimisation.
# Required for -fsanitize=cfi by clang, but a good idea at any rate.
addcflags -flto

# Enable control flow protection.
addcflags -fcf-protection=full -fsanitize=cfi

# Enable stack protection.
addcflags -fstack-protector -fstack-protector-strong \
          -fsanitize=safe-stack -fstack-security-check

# Protect against stack clashing attacks.
addcflags -fstack-clash-protection

# Enable hardening.
addcflags -fhardened

# Check for (some) undefined behaviour.
case ${CC-} in
(musl-*)    # musl-gcc and musl-clang sometimes have trouble linking UBSan.
            : ;;
(*)         addcflags -fsanitize=undefined -static-libubsan
esac

# Check for (some) addressing bugs.
case ${CC-} in
(musl-*)    # musl-gcc and musl-clang sometimes have trouble linking ASan.
            : ;;
(*)         addcflags -fsanitize=address -static-libasan \
                     -fsanitize=pointer-compare -fsanitize=pointer-subtract
esac

# Enable gcc's static code analyser.
# GCC's analyzer doesn't allow to suppress false positives.
#addcflags -fanalyzer

# Turn warnings into errors, but only for compilers that
# support suppressing errors with pragmas.
addcflags gcc clang icc -Werror -Werror-all -pedantic-errors

# Enable most warnings for most compilers.
addcflags -Wall -Wextra -Wpedantic

# Warn about integer overflow.
addcflags -Woverflow

# Prohibit casting away const-qualifiers.
addcflags -Wcast-qual -Wwrite-strings

# Make sure switch statements are safe and exhaustive.
addcflags -Wimplicit-fallthrough=2 -Wswitch -Wswitch-default

# Accept false positives when catching operations that may overflow a buffer.
addcflags -Wstringop-overflow=4

# Be stricter about functions.
addcflags -Wunused-function -Wunused-parameter -Wmissing-prototypes

# Be stricter about types.
addcflags -Wbad-function-cast -Wcast-align=strict -Wconversion -Wmain \
          -Wflex-array-member-not-at-end

# Be stricter about pointers.
addcflags -Wnonnull -Wnull-dereference

# Be stricter about variables.
addcflags -Wshadow -Wundef -Wuninitialized

# Be stricter about formats.
addcflags -Wformat=2 -Wformat-overflow=2 -Wformat-signedness \
          -Wformat-nonliteral -Wformat-security

# Try to catch logic errors.
addcflags -Wparentheses -Wlogical-op

# Enable all warning types for the Intel C compiler (ICC).
addcflags icc -w3

# Enable more warnings for ICC.
addcflags icc -Wcheck

# Disable uninformative warnings.
addcflags -Wno-unknown-warning-option -Wno-unused-command-line-argument \
          -Qunused-arguments

# Disable diagnostics for unknown pragmas for ICC,
# which claims to be compatible with GCC, but isn't.
addcflags icc -diag-disable=161,2282

# Disable ICC deprecation warning.
addcflags icc -diag-disable=10441

# Needed by icc to compile the test suite given -Werror-all.
addcflags icc -no-inline-max-size -no-inline-max-total-size

# Speed up compilation.
addcflags -pipe

# Make the global offset table read-only.
addldflags -Wl,-z,relro -Wl,-z,now

# Overwrite existing files.
force=x

# Load local settings.
if [ -e "$repodir/conf/local.env" ]
then . "$repodir/conf/local.env"
fi
