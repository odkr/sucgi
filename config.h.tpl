/*
 * suCGI configuration
 * ===================
 *
 * Defaults
 * --------
 *
 * Defaults can be found in params.h. Any macro in params.h can be
 * overriden by defining a macro of the same name in this file.
 *
 *
 * Syntax
 * ------
 *
 * This is a C header file.
 *
 * - #define statements are terminated with a linefeed, NOT a semicolon.
 * - * To continue a #define on the next line, escape the linefeed with "\".
 * - * Strings are given in double quotes ("..."), NOT single ones ('...').
 *
 * Copyright
 * ---------
 *
 * Copyright 2022, 2023, and 2024 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */


/* These are config.h's include guards. Leave them alone. They're on duty. */
#if !defined(PARAMS_H) && !defined(LINT)
#error config.h must not be included directly; include params.h instead.
#endif

#if !defined(CONFIG_H)
#define CONFIG_H

/*
 * The document root of user websites. Filename pattern.
 * CGI scripts are only run if they reside under their owner's document root.
 *
 * MUST correspond to the user directory set in your web server configuration.
 *
 * The following format specifiers are expanded:
 *      %n  Login name
 *      %d  Home directory
 *      %u  User ID
 *      %g  Primary group ID
 *      %%  Literal "%"
 */
/* #define USER_DIR "%d/public_html" */


/*
 * The base URL of user homepages, without the hostname. Filename pattern.
 * Use "/~%n" for Apache.
 *
 * Format specifiers are the same as for USER_DIR.
 */
/* #define BASE_URL "/~%n" */


/*
 * Range of user IDs reserverd for non-system users. Integers.
 *
 * Usually starts at 100, 500, or 1,000 and ends at 30,000 or 60,000.
 * [START_UID, STOP_UID] MUST be set to a subset of that range.
 *
 * Only CGI scripts owned by non-system users are executed by suCGI.
 */
/* #define START_UID 1000 */
/* #define STOP_UID 30000 */


/*
 * Range of group IDs reserved for non-system groups. Integers.
 *
 * Usually starts at 100, 500, or 1,000 and ends at 30,000 or 60,000.
 * [START_GID, STOP_GID] MUST be set to a subset of that range.
 *
 * On systems that make no such reservation,
 * exclude as many system groups as feasible.
 *
 * Only CGI scripts owned by users who are NOT a member of
 * any system group are executed are suCGI.
 */
/* #define START_GID 1000 */
/* #define STOP_GID 30000 */


/*
 * Path of the PHP-CGI executable. String literal.
 *
 * Searched for in the $PATH, unless it contains a slash.
 * But note that $PATH is set to PATH (see below).
 */
/*
#define PHP "php-cgi"
*/


/*
 * Handlers to run CGI scripts with.
 * Array of filename suffix-script handler pairs.
 *
 * The filename suffix must include the leading dot (e.g., ".php").
 * The script handler is searched for in $PATH,
 * unless its name contains a slash (e.g., "php").
 * But note that $PATH is set to PATH (see below).
 *
 * If no handler can be found, suCGI will execute the CGI script directly.
 */
/*
#define SCRIPT_HANDLERS { \
    {".php",   PHP}, \
    {".php8",  PHP}, \
    {".php7",  PHP}, \
    {".php5",  PHP}, \
    {".php4",  PHP}, \
    {".php3",  PHP}, \
    {".phtml", PHP}
}
*/


/*
 * Secure $PATH. String literal. Colon-separated list of directories.
 */
/*
#define PATH "/usr/bin:/bin"
 */


/*
 * Secure file permission mask. Unsigned integer.
 *
 * Permission masks are often given as octal numbers (e.g., 022 for go-w).
 * For a number to be interpreted as octal by the C compiler,
 * it must be prefixed with a zero (e.g., 022).
 */
/*
#define UMASK (S_ISUID | S_ISGID | S_ISVTX | S_IRWXG | S_IRWXO)
 */


/*
 * Priorities to log. Syslog constant.
 * See syslog(3) for details.
 */
/*
#define SYSLOG_MASK LOG_UPTO(LOG_ERR)
 */


#endif /* !defined(CONFIG_H) */
