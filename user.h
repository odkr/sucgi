/*
 * Header file for user.c
 *
 * Copyright 2024 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#if !defined(USER_H)
#define USER_H

#include <pwd.h>

#include "attr.h"
#include "types.h"


/*
 * Expand passwd entry specifiers in the given format string, copy the
 * resulting string to the memory area pointed to by "buf", which must
 * be at least of the given size, and save the length of the expanded
 * string in the variable pointed to by "len".
 *
 * Specifiers:
 *      %n  Login name
 *      %d  Home directory
 *      %u  User ID
 *      %g  Primary group ID
 *      %%  Literal '%'
 *
 * Return value:
 *      OK        Success
 *      ERR_ARGS  Unknown format specifier.
 *      ERR_BAD   Login name or home directory filename is too long.
 *      ERR_LEN   Resulting string is too long.
 */
_read_only(1) _read_only(2) _write_only(4, 3) _nonnull(1, 2, 4)
Error user_exp(const char *fmt, const struct passwd *user,
               size_t bufsize, char *buf, size_t *len);


#endif /* !defined(USER_H) */
