/*
 * Header file for str.c
 *
 * Copyright 2022 and 2023 Odin Kroeger.
 *
 * This file is part of suCGI.
 *
 * suCGI is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * suCGI is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with suCGI. If not, see <https://www.gnu.org/licenses/>.
 */

#if !defined(STR_H)
#define STR_H

#include <stdlib.h>

#include "attr.h"
#include "types.h"


/*
 * Split the given string at the first occurence of any of the given bytes,
 * and copy the substring up to, but not including, that byte to the memory
 * area pointed to by "head" and return a pointer to the substring that
 * starts after the separator in the memory area pointed to by "tail".
 *
 * The memory area pointed to by "head" must be of the given size;
 * the substring before the separator must be shorter than that size.
 *
 * Neither substring is meaningful if an error occurs.
 *
 * Return value:
 *      OK          Success.
 *      ERR_LEN     The substring before the separator is too long.
 */
_read_only(1) _read_only(2) _write_only(4, 3) _nonnull(1, 2, 4, 5) _nodiscard
Error str_split(const char *str, const char *sep,
                size_t size, char *head, const char **tail);


#endif /* !defined(STR_H) */
