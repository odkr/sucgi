# Installing suCGI

## Installation

Once you have [compiled](build.md) suCGI, install it with

    sudo make install

Note, this will only copy files that are newer than their target.


## Configuration variables

See the [GNU Coding Standard] (chap. 7) for details.

[GNU Coding Standard]: https://www.gnu.org/prep/standards

suCGI uses the following non-standard variables:

### Group the web server runs as (group name or ID)

| Tool           | Variable/macro name      |
| -------------  | ------------------------ |
| *configure*    | webgroup                 |
| Make           | webgroup                 |
| C Preprocessor | -                        |

Defaults to "www-data".


### **cgi-bin** directory (absolute filename)

| Tool           | Variable/macro name      |
| -------------  | ------------------------ |
| *configure*    | cgidir                   |
| Make           | cgidir                   |
| C Preprocessor | -                        |

Defaults to **/usr/lib/cgi-bin**.
