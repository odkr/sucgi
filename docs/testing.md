# Testing suCGI

## Overview

Run the test suite by:

    make check

It must be permitted to execute files in the repository
(i.e., it must not reside on a filesystem that is mounted noexec).

See below for details.


## Requirements

The test suite is less portable that suCGI itself.
It requires:

* a system that implements the "Spawn" and "Thread-Safe Functions"
  POSIX.1 extensions.
* the **<err.h>** header file and system interfaces.

In addition, `make distcheck` requires *tar* and [Pandoc].

[Pandoc]: https://pandoc.org/


## Security

`make check` will compile the test suite and utilities used by tests.
However, two of these utilities, *badenv* and *badexec*, have security
implications. *badenv* permits to call applications with maliciously
crafted environment variables. *badexec* calls applications with an
arbitrary *argv[0]*. If the system is set up so that users either
cannot compile applications or cannot run applications they compiled,
then compiling *badenv* or *badexec* may open up attacks vectors that
had previously been foreclosed.


## Organisation

There is a test suite for for *main()* and each function in **libsucgi.a**.
Test suites are orindary programmes; no testing framework is used.
They are located in the **tests** directory and have the same name as the
function they are the test suite of; long filenames are abbreviated, however.


## Exit statuses

| Status        | Meaning            |
| ------------- | ------------------ |
| 0             | Tests passed.      |
| 69            | An error occurred. |
| 70            | Tests failed.      |
| 75            | Tests was skipped. |

Any other exit status indicates a bug in the test suite.


## Running individual tests

Use

    make check checks="$tests"

to compile and run individual tests only, where `$tests` is
a whitespace-separated lists of filenames.


## Superuser privileges

suCGI requires superuser privileges. Consequently, so do some tests.
These will be skipped if the test suite is run without superuser privileges.
Conversely, some tests can only be run as a non-system user. Those will
be run as *arbitrary* non-system user if the test suite is run with
superuser privileges; note that the user with the permissions of whom
those tests are run can interfere with testing.

On systems that support shared objects, some tests that would otherwise
require superuser privileges can be run without superuser privileges by
`make mockcheck`.


## Testing different compilers, makes, and shells

**utils/checkcomps** tests different compilers.

**utils/checkmakes** tests different makes.

**utils/checkshells** tests different shells.
