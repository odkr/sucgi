# Uninstalling suCGI

You can uninstall suCGI by:

    sudo make uninstall

If you changed the **makefile**'s defaults by defining Make macros on the
command line when you installed suCGI, then you *must* define the same
macros with the same values when you call `make uninstall`.

For example, if suCGI was installed with `make prefix=/usr install`,
it *must* be uninstalled with `make prefix=/usr uninstall`.

However,

    ./configure prefix=/usr
    make
    sudo make install
    sudo make uninstall

is safe.
